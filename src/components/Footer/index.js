import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Container from "../Custom/Container";
import s from "./footer.module.css";
import img from "../../assets/img/logo-white.png";
import { MdPhoneInTalk } from "react-icons/md";
import { FaEnvelope, FaFacebookF, FaLinkedinIn } from "react-icons/fa";
import { Footer as words } from "../../utils/lang";

function Footer({ lang }) {
  return (
    <footer>
      <Container>
        <div className={s.content}>
          <div className={s.btn}>
            <Link to="/sign-up">{words[lang].btn}</Link>
          </div>
          <div className={s.logo}>
            <img src={img} alt="logo" />
          </div>
        </div>
        <div className={s.info}>
          <div className={s.policy}>
            <ul>
              <li>
                <Link to="/faq">{words[lang].faq}</Link>
              </li>
              <li>
                <Link to="/terms">{words[lang].terms}</Link>
              </li>
              <li>
                <Link to="/policy">{words[lang].privacy}</Link>
              </li>
            </ul>
          </div>
          <div className={s.social}>
            <ul>
              <li>
                <MdPhoneInTalk size={16} color="#fff" />
                <a href="tel:+37455838338">+37455838338</a>
              </li>
              <li>
                <FaEnvelope size={14} color="#fff" />
                <a href="mailto:info@ucar.am">info@ucar.am</a>
              </li>
              <li>
                <FaFacebookF size={17} color="#fff" />
              </li>
              <li>
                <FaLinkedinIn size={18} color="#fff" />
              </li>
            </ul>
          </div>
        </div>
      </Container>
    </footer>
  );
}
Footer.propTypes = {
  lang: PropTypes.string
};

Footer.defaultProps = {
  lang: "hy"
};

export default Footer;
