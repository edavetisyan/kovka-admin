import React from "react";
import PropTypes from "prop-types";
import s from "./modal.module.css";
import Button from "../Custom/Button";

const Modal = ({
  children,
  onClose,
  cancel,
  title,
  text,
  submitText,
  onSubmit
}) => {
  return (
    <div className={s.root}>
      <div className={s.content}>
        <h4>{title}</h4>
        <p>{text}</p>
        <>{children}</>
        <ul>
          {cancel && (
            <li>
              <Button theme="primary" size="sm" onClick={onClose}>
                {cancel}
              </Button>
            </li>
          )}
          {submitText && (
            <li>
              <Button theme="danger" size="sm" onClick={onSubmit}>
                {submitText}
              </Button>
            </li>
          )}
        </ul>
      </div>
    </div>
  );
};

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  submitText: PropTypes.string,
  onSubmit: PropTypes.func,
  title: PropTypes.string,
  text: PropTypes.string,
  cancel: PropTypes.string,
  children: PropTypes.node
};

Modal.defaultProps = {
  title: "",
  text: "",
  cancel: "Cancel",
  onSubmit: () => false,
  submitText: "",
  children: null
};

export default Modal;
