/* eslint-disable no-undef */
import React, { useCallback, useState, useMemo } from "react";
import s from "./info.module.css";
import PropTypes from "prop-types";
import image from "../../assets/img/images.png";
import Container, { Button, Status } from "../../components/Custom";
import { Row } from "react-flexbox-grid";
import {
  WAITING,
  UNCONFIRMED,
  UNCOMPLETED,
  ACTIVE
} from "../../constants/user";
import fetchData from "../../utils/fetchData";
import { useFetch } from "../../hooks/useFetch";

function UserInfo({ id }) {
  const user = useFetch(`user/clients/${id}`);
  const [active, setActive] = useState(false);
  const [status, setStatus] = useState(user.status);

  const handleClick = useCallback(() => {
    fetchData(`${process.env.REACT_APP_API_URL}user/confirm-driver`, {
      body: { userId: user.id }
    })
      .then(res => {
        setActive(true);
        setStatus(ACTIVE);
      })
      .catch(err => {
      });
  }, [user.id]);

  const statusText = useMemo(() => {
    switch (status) {
      case UNCONFIRMED:
        return <Status theme="danger">UNCONFIRMED</Status>;
      case UNCOMPLETED:
        return <Status theme="danger">UNCOMPLETED</Status>;
      case WAITING:
        return <Status theme="primary">WAITING</Status>;
      case ACTIVE:
        return <Status theme="success">ACTIVE</Status>;
      default:
        return "";
    }
  }, [status]);

  return (
    <Container>
      <div className={s.info}>
        <div className={s.wrapp}>
          <div className={s.avatar}>
            <img
              src={
                user.img
                  ? `${process.env.REACT_APP_API_URL}upload/${user.image}`
                  : image
              }
              alt="User"
            />
          </div>
          <div className={s.about}>
            <h3>
              {user.firstname} {user.lastname}
            </h3>
            {/* <span>{day}</span> */}
            <p>{user.email}</p>
            <p>{user.phone}</p>
            <p> Status: {statusText}</p>
          </div>
        </div>
        <div className={s.driver}>
          <div className={s.aboutDriver}>
            <h4>Car Mark:</h4>
            {/* <span>{mark}</span> */}
          </div>
          <div className={s.aboutDriver}>
            <h4>Car Model:</h4>
            {/* <span>{model}</span> */}
          </div>
          <div className={s.aboutDriver}>
            <h4>Car Size:</h4>
            {/* <span>{size}</span> */}
          </div>
          <div className={s.aboutDriver}>
            <h4>Price KM:</h4>
            {/* <span>{price} AMD</span> */}
          </div>
        </div>
      </div>
      {user.status === WAITING && !active && (
        <Row end="md">
          <Button onClick={handleClick}>Confirm</Button>
        </Row>
      )}
    </Container>
  );
}
UserInfo.propTypes = {
  id: PropTypes.string.isRequired
};
export default UserInfo;
