import React, { useContext, useCallback, useState } from "react";
import { Link as Scroll, scroller } from "react-scroll";
import { Link } from "react-router-dom";
import { FaUserAlt } from "react-icons/fa";
import Container from "../../Custom/Container";
import img from "../../../assets/img/logo-white.png";
import s from "./header.module.css";
import { SettingsContext } from "../../../stores/settings";
import words from "../../../utils/words";

function Header() {
  const [active, setActive] = useState("");

  const {
    state: { language },
  } = useContext(SettingsContext);

  const handleSetActive = useCallback(to => {
    setActive(to);
    return scroller.scrollTo(to, {
      duration: 1500,
      // delay: 100,
      smooth: true
    });
  }, []);

  return (
    <header>
      <Container>
        <div className={s.wrap}>
          <div className={s.logo}>
            <Link to="/" >
              <img src={img} alt="logo" />
            </Link>
          </div>
          <div className={s.menu}>
            <ul>
              {window.location.pathname === "/" && (
                <ul>
                  {words[language].header.menu.map(element => {
                    return (
                      <li key={element} className={`${active === element ? 'active' : ''}`}>
                        <Scroll to={element} onClick={() => handleSetActive(element)}>{element}</Scroll>
                      </li>
                    );
                  })}
                </ul>
              )}
            </ul>
            <div className={s.btn}>
              <Link to="/sign-in">
                <FaUserAlt color="#003C80" size={14} />
                <span> {words[language].header.btn}</span>
              </Link>
            </div>
          </div>
        </div>
      </Container>
    </header>
  );
}
export default Header;
