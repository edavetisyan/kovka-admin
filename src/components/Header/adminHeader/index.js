import React from "react";
import { Link } from "react-router-dom";
import { Col, Row } from "react-flexbox-grid";
import { FaSignOutAlt } from "react-icons/fa";
import s from "./header.module.css";
import logo from "../../../assets/img/logo-white.png";

function AdminHeader() {
  return (
    <div className={s.header}>
      <Row between="md" center="md">
        <Col>
          <Link to="/dashboard">
            <img src={logo} className={s.logo} alt="ReRid" />
          </Link>
        </Col>
        <Col>
          <Link to="/logout">
            <div className={s.listItemText}>
              <FaSignOutAlt size={17} color="#fff" /> <span>Logout</span>
            </div>
          </Link>
        </Col>
      </Row>
    </div>
  );
}

export default AdminHeader;
