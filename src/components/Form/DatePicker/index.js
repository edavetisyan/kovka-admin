import React, { memo, useCallback, useContext } from "react";
import PropTypes from "prop-types";
import DatePicker from "react-datepicker";
import classNames from "classnames";
import { FormContext } from "../../../stores/form";
import { SET_VALUE } from "../../../constants/form";
import s from "./date.module.css";
import "react-datepicker/dist/react-datepicker.css";

function DatePickerComponent({ name, validation, ...rest }) {
  const {
    state: {
      elements: {
        [name]: { value, error }
      },
      showErrors
    },
    dispatch
  } = useContext(FormContext);
  const handleChange = useCallback(
    text => {
      dispatch({
        type: SET_VALUE,
        payload: {
          name,
          text: text,
          error: validation && !validation.test(text)
        }
      });
    },
    [dispatch, name, validation]
  );
  return (
    <DatePicker
      className={classNames(
        s.input,
        error && (showErrors || value) ? s.error : {}
      )}
      selected={value}
      showTimeSelect
      dateFormat="dd/mm/yyyy HH:mm"
      onChange={handleChange}
      {...rest}
    />
  );
}

DatePickerComponent.propTypes = {
  name: PropTypes.string.isRequired,
  validation: PropTypes.instanceOf(RegExp)
};

DatePickerComponent.defaultProps = {
  validation: new RegExp("")
};

export default memo(DatePickerComponent);
