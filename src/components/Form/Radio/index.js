/* eslint-disable no-undef */
import React from "react";
import PropTypes from "prop-types";
// import classNames from "classnames";
import s from "./radio.module.scss";
// import image from "../../../assets/img/ararat.jpg";

function Radio({ img, name, size, width, height, handleChange, ...rest }) {
  return (
    <div className={s.radio}>
      <input
        type="radio"
        id={rest.value}
        name={name}
        {...rest}
        onChange={handleChange}
      />
      <label htmlFor={rest.value}>
        <img
          src={`${process.env.REACT_APP_API_URL}upload/${img}`}
          alt="car"
          width={width}
          height={height}
        />
      </label>
      {!!size && <h4> {size}</h4>}
    </div>
  );
}

Radio.propTypes = {
  size: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  img: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  handleChange: PropTypes.func
};
Radio.defaultProps = {
  size: "",
  name: "",
  img: "",
  value: "",
  width: "",
  height: "",
  handleChange: () => false
};

export default Radio;
