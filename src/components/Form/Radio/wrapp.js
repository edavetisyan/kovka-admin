/* eslint-disable no-undef */
import React from "react";
import s from "./radio.module.scss";

// eslint-disable-next-line react/prop-types
function RadioWrapp({ children }) {
  return <div className={s.RadioWrapp}>{children}</div>;
}

export default RadioWrapp;
