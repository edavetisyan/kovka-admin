/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
import React, { memo, useCallback, useState, useContext } from "react";
import PlacesAutocomplete, {
  geocodeByPlaceId,
  getLatLng
} from "react-places-autocomplete";
import { FormContext } from "../../../stores/form";
import { SET_VALUE } from "../../../constants/form";

import s from "../Input/input.module.scss";

function AddressLine({ id, name, handleChange, placeholder, validation }) {
  const [addressError, setAddressError] = useState(false);
  const {
    state: {
      elements: {
        [name]: { value, error }
      },
      showErrors
    },
    dispatch
  } = useContext(FormContext);

  const [address, setAddress] = useState(value || "");
  const handleSelectAddress = useCallback(
    (result, type) => {
      dispatch({
        type: SET_VALUE,
        payload: {
          name,
          text: result,
          error: validation && !validation.test(result)
        }
      });
      setAddress(result);
      let fullAddress = {};
      geocodeByPlaceId(type)
        .then(async res => {
          fullAddress = { ...res[0] };
          return getLatLng(res[0]);
        })
        .then(latLng => {
          fullAddress.geometry.location = latLng;
          setAddressError(false);
          handleChange(name, { ...fullAddress, address });
        })
        .catch(err => {
          // setAddressError(true);
        });
    },
    [address, dispatch, handleChange, name, validation]
  );

  const onAddressError = useCallback((status, clearSuggestions) => {
    clearSuggestions();
  }, []);
  const searchOptions = {
    types: ["address"],
    componentRestrictions: {
      country: ["am"]
    }
  };
  return (
    <PlacesAutocomplete
      onChange={setAddress}
      onSelect={handleSelectAddress}
      onError={onAddressError}
      searchOptions={searchOptions}
      value={address}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps }) => (
        <>
          <input
            id={id}
            name={name}
            placeholder={placeholder}
            {...getInputProps({
              className: `location-search-input ${
                addressError === undefined
                  ? ""
                  : error && (showErrors || value)
                  ? s.error
                  : ""
              } ${s.input}`
            })}
          />
          <div className="autocomplete-dropdown-container">
            {suggestions.map(suggestion => {
              const className = suggestion.active
                ? "suggestion-item--active"
                : "suggestion-item";
              // inline style for demonstration purpose
              const style = suggestion.active
                ? { backgroundColor: "#fafafa", cursor: "pointer" }
                : { backgroundColor: "#ffffff", cursor: "pointer" };
              return (
                <div
                  {...getSuggestionItemProps(suggestion, { className, style })}
                >
                  <span>{suggestion.description}</span>
                </div>
              );
            })}
          </div>
        </>
      )}
    </PlacesAutocomplete>
  );
}

export default memo(AddressLine);
