import React, {
  useMemo,
  useCallback,
  memo,
  useContext,
  useEffect
} from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import classNames from "classnames";
import s from "./select.module.css";
import { FormContext } from "../../../stores/form";
import { SET_VALUE } from "../../../constants/form";
const SelectComponent = ({
  options,
  isMulti,
  validation,
  name,
  handleChange,
  isDisabled,
  ...rest
}) => {
  const {
    state: {
      elements: {
        [name]: { value, error }
      },
      showErrors
    },
    dispatch
  } = useContext(FormContext);
  useEffect(() => {
    if (rest.initialValue && !value) {
      dispatch({
        type: SET_VALUE,
        payload: {
          name,
          text: rest.initialValue,
          error: validation && !validation.test(rest.initialValue)
        }
      });
    }
  }, [dispatch, name, options, rest.initialValue, validation, value]);
  const selectedValue = useMemo(() => {
    if (isMulti) {
      return options.filter(item => value.includes(item.value));
    }
    return options.find(item => item.value === value);
  }, [isMulti, options, value]);

  const handleSelect = useCallback(
    e => {
      let select = null;
      if (e) {
        if (isMulti) {
          select = e.map(item => item.value);
        } else {
          select = e.value;
        }
      } else {
        select = e;
      }
      dispatch({
        type: SET_VALUE,
        payload: {
          name,
          text: select,
          error: validation && !validation.test(select)
        }
      });
      handleChange(e);
    },
    [dispatch, handleChange, isMulti, name, validation]
  );
  return (
    <Select
      options={options}
      onChange={handleSelect}
      className={classNames(
        s.select,
        error && (showErrors || selectedValue) ? s.error : ""
      )}
      value={selectedValue}
      isMulti={isMulti}
      isDisabled={isDisabled}
      {...rest}
    />
  );
};

SelectComponent.propTypes = {
  value: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  name: PropTypes.PropTypes.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })
  ).isRequired,
  handleChange: PropTypes.func,
  isDisabled: PropTypes.bool,
  isMulti: PropTypes.bool,
  validation: PropTypes.instanceOf(RegExp)
};

SelectComponent.defaultProps = {
  // value: {}
  isDisabled: false,
  isMulti: false,
  validation: RegExp(""),
  handleChange: () => false
};

export default memo(SelectComponent);
