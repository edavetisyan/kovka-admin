import React, { useContext, useCallback, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { FormContext } from "../../../stores/form";
import { SET_VALUE } from "../../../constants/form";
import s from "./check.module.css";

function CheckBox({ name, size,  ...rest }) {
  const [check, setChack ] = useState(rest.initialValue || false)
  const {
    dispatch
  } = useContext(FormContext);
  
  useEffect(() => {
    dispatch({
      type: SET_VALUE,
      payload: {
        name,
        text: check,
      }
    });
    
  }, [dispatch, check, name])

  const handleChange = useCallback(
    () => {
      dispatch({
        type: SET_VALUE,
        payload: {
          name,
          text: setChack(!check),
        }
      });
    },
    [dispatch, name, check]
  );
  return (
    <div className={s.check}>
      <input
        type="checkbox"
        id={rest.id}
        name={name}
        checked={check}
        {...rest}
        onChange={handleChange}
      />
      <label htmlFor={rest.id}></label>
    </div>
  );
}

CheckBox.propTypes = {
  size: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  handleChange: PropTypes.func
};
CheckBox.defaultProps = {
  size: "",
  name: "",
  value: "",
  handleChange: () => false
};

export default CheckBox;
