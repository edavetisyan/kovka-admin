import React, { useCallback, memo, useContext } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { FormContext } from "../../../stores/form";
import { SET_VALUE } from "../../../constants/form";
import s from "./input.module.scss";

function Input(props) {
  const { search, validation, name, type, readonly, ...rest } = props;
  const {
    state: {
      elements: {
        [name]: { value, error }
      },
      showErrors
    },
    dispatch
  } = useContext(FormContext);

  const handleChange = useCallback(
    text => {
      dispatch({
        type: SET_VALUE,
        payload: {
          name,
          text: text.target.value,
          error: validation && !validation.test(text.target.value)
        }
      });
    },
    [dispatch, name, validation]
  );
  return (
    <input
      className={classNames(
        s.input,
        error && (showErrors || value) ? s.error : {},
        search ? s.search : {}
      )}
      onChange={handleChange}
      readOnly={readonly}
      value={value}
      type={type}
      {...rest}
    />
  );
}

Input.propTypes = {
  name: PropTypes.string.isRequired,
  validation: PropTypes.instanceOf(RegExp),
  // errorMessage: PropTypes.string,
  type: PropTypes.string,
  readonly: PropTypes.bool,
  search: PropTypes.bool
};

Input.defaultProps = {
  validation: new RegExp(""),
  // errorMessage: "incorrect",
  type: "",
  readonly: false,
  search: false
};

export default memo(Input);
