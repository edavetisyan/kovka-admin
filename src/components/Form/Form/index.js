/* eslint-disable no-prototype-builtins */
import React, { useCallback, useMemo, useContext } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { Row, Col } from "react-flexbox-grid";
import { FormContextProvider, FormContext } from "../../../stores/form";
import {
  Input,
  PhoneInput,
  Button,
  Textarea,
  Select,
  AddressLine,
  DatePicker,
  CheckBox
} from "../index";

import { SHOW_ERRORS } from "../../../constants/form";
import Group from "../Group";
import s from "./form.module.scss";

function FormBuilder({ content, isLoading, button, onSubmit }) {
  const { state, dispatch } = useContext(FormContext);
  const handleSubmit = useCallback(
    e => {
      e.preventDefault();
      dispatch({ type: SHOW_ERRORS });
      const values = Object.values(state.elements);

      const isValid = values.every(item => !item.error);
      if (isValid) {
        const data = {};
        for (let key in state.elements) {
          if (!state.elements.hasOwnProperty(key)) continue;
          data[key] = state.elements[key].value;
        }

        onSubmit(data);
      }
    },
    [dispatch, onSubmit, state.elements]
  );

  const componentsArray = useMemo(() => {
    const { ...buttonRest } = button;
    return (
      <>
        <form onSubmit={handleSubmit}>
          <Row>
            {content.map((item, key) => {
              const { ...rest } = item;
              switch (item.componentType) {
                case "input":
                  return (
                    <Col key={key} sm={item.size}>
                      <Group label={rest.label}>
                        <Input  {...rest} />
                      </Group>
                    </Col>
                  );
                case "textarea":
                  return (
                    <Col key={key} sm={item.size}>
                      <Group label={rest.label}>
                        <Textarea  {...rest} />
                      </Group>
                    </Col>
                  );
                case "phone":
                  return (
                    <Col key={key} sm={item.size}>
                      <Group label={rest.label}>
                        <PhoneInput key={key}  {...rest} />
                      </Group>
                    </Col>
                  );
                case "select":
                  return (
                    <Col key={key} sm={item.size}>
                      <Group label={rest.label}>
                        <Select  {...rest} />
                      </Group>
                    </Col>
                  );
                case "address":
                  return (
                    <Col key={key} sm={item.size}>
                      <Group label={rest.label}>
                        <AddressLine {...rest} />
                      </Group>
                    </Col>
                  );
                case "check":
                  return (
                    <Col key={key} sm={item.size}>
                      <Group label={rest.label}>
                        <CheckBox {...rest} />
                      </Group>
                    </Col>
                  );
                case "date":
                  return (
                    <Col key={key} sm={item.size}>
                      <Group label={rest.label}>
                        <DatePicker {...rest} />
                      </Group>
                    </Col>
                  );
                case "button":
                  return (
                    <Col key={key} sm={item.size}>
                      <div className={s.buttonContStyle} key={key}>
                        <Button type="submit" {...rest}>
                          {rest.title}
                        </Button>
                      </div>
                    </Col>
                  );
                default:
                  throw new Error("unhandled component type");
              }
            })}
          </Row>
          {!_.isEmpty(button) && (
            <div className={s.buttonContStyle}>
              <Button isLoading={isLoading} type="submit" {...buttonRest}>
                {button.title}
              </Button>
            </div>
          )}
        </form>
      </>
    );
  }, [button, handleSubmit, content, isLoading]);
  return <div className={s.root}>{componentsArray}</div>;
}

FormBuilder.propTypes = {
  content: PropTypes.arrayOf(PropTypes.object).isRequired,
  onSubmit: PropTypes.func.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  button: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool
};

FormBuilder.defaultProps = {
  style: {},
  button: {},
  isLoading: false
};

function Form(props) {
  const { content } = props;
  const inputs = useMemo(() => {
    const elements = {};

    const inputsArray = content
      .filter(
        item =>
          item.componentType === "input" ||
          item.componentType === "phone" ||
          item.componentType === "textarea" ||
          item.componentType === "select" ||
          item.componentType === "date" ||
          item.componentType === "check" ||
          item.componentType === "address"
      )
      .map(item => {
        elements[item.name] = {
          value: item.initialValue || "",
          error: !!item.isRequired && !item.initialValue
        };

        return item.name;
      });
    return { elements, inputsArray };
  }, [content]);

  return (
    <FormContextProvider {...inputs}>
      <FormBuilder {...props} />
    </FormContextProvider>
  );
}

Form.propTypes = {
  content: PropTypes.arrayOf(PropTypes.object).isRequired
};

FormBuilder.propTypes = {
  rowCenter: PropTypes.string
};
FormBuilder.defaultProps = {
  rowCenter: "start"
};

export default Form;
