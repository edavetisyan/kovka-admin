import React, { useCallback, useMemo, memo, useContext, useRef } from "react";
import InputMask from "react-input-mask";
import classNames from "classnames";
import PropTypes from "prop-types";
import { FormContext } from "../../../stores/form";
import { SET_VALUE, CHANGE_FOCUS } from "../../../constants/form";
import s from "./input.module.scss";

function Input(props) {
  const { search, name, validation, ...rest } = props;
  const {
    state: {
      focused,
      elements: {
        [name]: { value, error }
      },
      showErrors
    },
    dispatch
  } = useContext(FormContext);

  const ref = useRef(false);
  useMemo(() => {
    if (focused === name) {
      ref.current.focus();
    }
  }, [focused, name]);
  const handleChange = useCallback(
    text => {
      dispatch({
        type: SET_VALUE,
        payload: {
          name,
          text: text.target.value,
          error: validation && !validation.test(text.target.value)
        }
      });
    },
    [dispatch, name, validation]
  );

  const handleSubmitEditing = useCallback(() => {
    dispatch({ type: CHANGE_FOCUS, payload: { name } });
  }, [dispatch, name]);

  // eslint-disable-next-line no-unused-vars
  return (
    <InputMask
      className={classNames(
        s.input,
        error && (showErrors || value) ? s.error : {},
        search ? s.search : {}
      )}
      onChange={handleChange}
      maskChar={null}
      value={value}
      // onFocus={onInputFocus}
      mask="+(374) 99-99-99"
      onSubmitEditing={handleSubmitEditing}
      {...rest}
    />
  );
}

Input.propTypes = {
  name: PropTypes.string.isRequired,
  errorMessage: PropTypes.string,
  validation: PropTypes.instanceOf(RegExp),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  search: PropTypes.bool
};

Input.defaultProps = {
  validation: new RegExp(""),
  errorMessage: "Phone number is incorrect",
  search: false
};

export default memo(Input);
