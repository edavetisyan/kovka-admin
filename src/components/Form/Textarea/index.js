import React, { useCallback, memo, useContext } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { FormContext } from "../../../stores/form";
import { SET_VALUE } from "../../../constants/form";
import s from "./textarea.module.css";

function Textarea(props) {
  const { search, validation, name, readonly, ...rest } = props;
  const {
    state: {
      elements: {
        [name]: { value, error }
      },
      showErrors
    },
    dispatch
  } = useContext(FormContext);

  const handleChange = useCallback(
    text => {
      dispatch({
        type: SET_VALUE,
        payload: {
          name,
          text: text.target.value,
          error: validation && !validation.test(text.target.value)
        }
      });
    },
    [dispatch, name, validation]
  );

  return (
    <textarea
      className={classNames(
        s.input,
        error && (showErrors || value) ? s.error : {},
        search ? s.search : {}
      )}
      onChange={handleChange}
      readOnly={readonly}
      value={value}
      {...rest}
    />
  );
}

Textarea.propTypes = {
  name: PropTypes.string.isRequired,
  validation: PropTypes.instanceOf(RegExp),
  // errorMessage: PropTypes.string,
  readonly: PropTypes.bool,
  search: PropTypes.bool
};

Textarea.defaultProps = {
  validation: new RegExp(""),
  // errorMessage: "incorrect",
  readonly: false,
  search: false
};

export default memo(Textarea);
