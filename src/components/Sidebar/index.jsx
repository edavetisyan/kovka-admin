/* eslint-disable no-undef */
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../../stores/auth";
import {
  FaTachometerAlt,
  FaKickstarter,
  FaAddressCard,
  FaListAlt,
  FaProductHunt
} from "react-icons/fa";
// import { ADMIN, CLIENT } from "../../constants/user";
import s from "./sidebar.module.css";

const menu = {
  size: [
    {
      name: "домашняя страница",
      icon: <FaTachometerAlt size="1.2em" />,
      to: "/dashboard"
    },
    {
      name: "Продукт",
      icon: <FaProductHunt size="1.2em" />,
      to: "/product"
    },
    {
      name: "категория",
      icon: <FaListAlt size="1.2em" />,
      to: "/category"
    },
    {
      name: "Подкатегория",
      icon: <FaListAlt size="1.2em" />,
      to: "/sub-category"
    },
    {
      name: "Отзывы",
      icon: <FaKickstarter size="1.2em" />,
      to: "/reviews"
    },
    {
      name: "контакт",
      icon: <FaAddressCard size="1.2em" />,
      to: "/conatct"
    },
  ],
};

const Sidebar = () => {
  const {
    state: {authenticated}
  } = useContext(AuthContext);
  return (
    <ul className={s["sidebar-list"]}>
      {!!authenticated &&
        menu.size.map(item => (
          <li
            key={item.to}
            className={window.location.pathname === item.to ? s.active : ""}
          >
            <Link to={item.to}>
              <div className={s.listItemText}>
                <p>
                  {item.icon}
                  <span>{item.name}</span>
                </p>
              </div>
            </Link>
            {item.sub && (
              <ul>
                {item.sub.map(sub => (
                  <li key={sub.to}>
                    <Link to={sub.to}>
                      <div className={s.listItemText}>
                        <p>
                          {sub.icon}
                          <span>{sub.name}</span>
                        </p>
                      </div>
                    </Link>
                  </li>
                ))}
              </ul>
            )}
          </li>
        ))}
    </ul>
  );
};

export default Sidebar;
