import React, { useEffect, useCallback, useState, useRef, forwardRef, useImperativeHandle } from "react";
import s from "./show.module.css";

function ShowError(props, ref) {
  const [opened, setOpened] = useState(false);
  const wrapperRef = useRef(false);

  const handleClickOutside = useCallback(event => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      setOpened(false);
    }
  }, []);

  useEffect(() => {
    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, [handleClickOutside]);

  const setWrapperRef = node => {
    wrapperRef.current = node;
  };

  useImperativeHandle(ref, () => ({
    show: () => setOpened(true),
    hide: () => setOpened(false)
  }));

  if (!opened) return null;
  return (
    <div className={s.show} ref={setWrapperRef}>
      <p>Լրացրեք բոլոր դաշտերը</p>
    </div>
  );
}

export default forwardRef(ShowError);
