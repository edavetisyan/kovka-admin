import React from "react";
import { GoogleMap, Marker, withGoogleMap } from "react-google-maps";

const MyMapComponent = withGoogleMap(({ isMarkerShown }) => {
  return (
    <div>
      <GoogleMap defaultZoom={8} defaultCenter={{ lat: -34.397, lng: 150.644 }}>
        {isMarkerShown && <Marker position={{ lat: -34.397, lng: 150.644 }} />}
      </GoogleMap>
    </div>
  );
});
export default MyMapComponent;
