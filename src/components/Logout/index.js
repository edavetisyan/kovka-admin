import React, { useEffect } from "react";
import storage from "../../utils/storage";
import history from "../../utils/history";
import Loading from "../Loading";

function Logout() {
  useEffect(() => {
    storage.remove("token");
    history.push("/");
  }, []);
  return (
    <div>
      <Loading />
    </div>
  );
}
export default Logout;
