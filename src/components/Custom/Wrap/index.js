import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import s from "./wrap.module.css";

function Wrap({ children }) {
  return <div className={classNames(s.wrap)}>{children}</div>;
}
Wrap.propTypes = {
  children: PropTypes.node.isRequired
};

Wrap.defaultProps = {
  //   checkout: false,
};
export default Wrap;
