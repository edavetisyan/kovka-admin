import React from "react";
import PropTypes from "prop-types";
import style from "./container.module.css";

function Container({ children }) {
  return <div className={style.root}>{children}</div>;
}
Container.propTypes = {
  children: PropTypes.node
};

Container.defaultProps = {
  children: null
};
export default Container;
