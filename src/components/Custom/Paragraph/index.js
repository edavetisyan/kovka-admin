import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import s from "./par.module.css";

const Paragraph = ({ children, center }) => (
  <div className={classNames(s.text, center ? s.center : {})}>
    <p>{children}</p>
  </div>
);
Paragraph.propTypes = {
  children: PropTypes.node.isRequired,
  center: PropTypes.bool
};
Paragraph.defaultProps = {
  center: false
};

export default Paragraph;
