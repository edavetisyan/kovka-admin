import React from 'react';
import s from './box.module.css';
import PropTypes from 'prop-types';
import classNames from 'classnames';

function Box({ children }) {
  return (
    <div className={classNames(s.box)}>
      {children}
    </div>
  )
}

Box.propTypes = {
  children: PropTypes.node,
};

Box.defaultProps = {
  children: null,
}
export default Box;