import React from "react";
import PropTypes from "prop-types";
import s from "./title.module.css";
import classNames from "classnames";

function Title({ title, color }) {
  return (
    <div className={s.root}>
      <h2 className={classNames(s.title, color ? s.white : {})}>{title}</h2>
    </div>
  );
}
Title.propTypes = {
  title: PropTypes.string.isRequired,
  color: PropTypes.bool
};
Title.defaultProps = {
  color: false
};
export default Title;
