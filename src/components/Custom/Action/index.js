import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import s from "./action.module.scss";
import classNames from "classnames";

function Action({ text, menu, style }) {
  return (
    <div className={classNames(s.list, style)}>
      {!!text && (
        <div className={s.listText}>
          <p>{text}</p>
        </div>
      )}
      <div className={s.menu}>
        <ul>
          {menu.map(element => {
            return (
              <li key={element.name}>
                {element.external ? (
                  <a href={element.href}>{element.name}</a>
                ) : (
                  <>
                    {element.href ? (
                      <Link to={element.href}>{element.name}</Link>
                    ) : (
                      <p>{element.name}</p>
                    )}
                  </>
                )}
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}
Action.propTypes = {
  text: PropTypes.string,
  style: PropTypes.object,
  menu: PropTypes.objectOf(PropTypes.string)
};
Action.defaultProps = {
  menu: [],
  style: {},
  text: ""
};

export default Action;