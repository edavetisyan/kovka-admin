/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-no-target-blank */
import React from "react";
// eslint-disable-next-line no-unused-vars
import PropTypes from "prop-types";
import classNames from "classnames";

import s from "./status.module.css";

const Status = ({ children, theme }) => {
  const classes = classNames(s.root, {
    [s.primary]: theme === "primary",
    [s.success]: theme === "success",
    [s.secondary]: theme === "secondary",
    [s.danger]: theme === "danger"
  });
  return <div className={classes}>{children}</div>;
};

export default Status;
