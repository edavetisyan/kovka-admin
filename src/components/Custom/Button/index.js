/* eslint-disable react/jsx-no-target-blank */
import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import classNames from "classnames";

import s from "./button.module.css";

const sizes = {
  SMALL: "sm",
  MEDIUM: "md",
  LARGE: "lg"
};

const themes = ["primary", "success", "secondary", "danger"];

const Button = ({
  children,
  size,
  // eslint-disable-next-line no-unused-vars
  circle,
  theme,
  fullWidth,
  readOnly,
  link,
  onClick,
  type,
  ...rest
}) => {
  const classes = classNames(s.root, {
    [s.sm]: size === sizes.SMALL,
    [s.md]: size === sizes.MEDIUM,
    [s.lg]: size === sizes.LARGE,
    [s.fullWidth]: fullWidth,
    [s.primary]: theme === "primary",
    [s.success]: theme === "success",
    [s.secondary]: theme === "secondary",
    [s.danger]: theme === "danger",
    [s.readOnly]: readOnly
  });

  if (link) {
    if (link.external) {
      return (
        <a href={link.href} className={classes} target="_blank">
          {children}
        </a>
      );
    }

    return (
      <Link
        to={link.href}
        className={classes}
        style={{
          display: "inline-flex",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {children}
      </Link>
    );
  }

  return (
    <button
      className={classes}
      type={type}
      disabled={readOnly}
      onClick={onClick}
      {...rest}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  size: PropTypes.oneOf([sizes.SMALL, sizes.MEDIUM, sizes.LARGE]),
  theme: PropTypes.oneOf(themes),
  fullWidth: PropTypes.bool,
  circle: PropTypes.bool,
  readOnly: PropTypes.bool,
  link: PropTypes.shape({
    href: PropTypes.string,
    external: PropTypes.bool
  }),
  onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.instanceOf(null)]),
  type: PropTypes.string
};

Button.defaultProps = {
  size: sizes.MEDIUM,
  theme: themes[0],
  circle: false,
  fullWidth: false,
  readOnly: false,
  link: null,
  onClick: null,
  type: "button"
};

export default Button;
