import GoogleMap from "./Map";
import Marker from "./Marker";
import TruckMarker from "./TruckMarker";
import MapPolyline from "./MapPolyline";
import CarMarker from "./CarMarker";

export default GoogleMap;
export { Marker, MapPolyline, TruckMarker, CarMarker };
