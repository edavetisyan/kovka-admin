import React from "react";

const Marker = ({ pickup }) => (
  <div className={`map-marker`}>
    <svg viewBox="0 0 18 25">
      <defs>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="bgDest">
          <stop stopColor="#4956EE" offset="0%" />
          <stop stopColor="#25A0F8" offset="100%" />
        </linearGradient>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="bgOrigin">
          <stop stopColor="#F24A7E" offset="0%" />
          <stop stopColor="#F9802F" offset="100%" />
        </linearGradient>
      </defs>
      <g strokeWidth={1} stroke="none" fill="none" fillRule="evenodd">
        <rect
          id="Rectangle"
          fill="#191919"
          x={8}
          y={16}
          width={2}
          height={9}
          rx={1}
        />
        <g>
          <rect
            fill={`url(#${pickup ? "bgOrigin" : "bgDest"})`}
            x={0}
            y={0}
            width={18}
            height={18}
            rx={6}
          />
          <g transform="translate(9.000000, 9.000000) rotate(-180.000000) translate(-9.000000, -9.000000) translate(3.000000, 3.000000)">
            <rect x={0} y={0} width={12} height={12} />
            <path
              d="M5.56774935,7.82518921 L3.16524282,5.2503489 C2.94491906,5.01422114 2.94491906,4.62792793 3.16524282,4.3920688 L3.36576501,4.17689435 C3.58558747,3.94103522 3.94627676,3.94103522 4.16634987,4.17689435 L5.99987467,6.1419394 L7.83365013,4.17689435 C8.05372324,3.94103522 8.41441253,3.94103522 8.63423499,4.17689435 L8.83475718,4.3920688 C9.05508094,4.62792793 9.05508094,5.01422114 8.83475718,5.2503489 L6.43174935,7.82518921 C6.31369191,7.95198364 6.15502872,8.00839642 5.99987467,7.99899429 C5.84472063,8.00839642 5.68630809,7.95198364 5.56774935,7.82518921 Z"
              fill="#FFFFFF"
              transform={
                !pickup
                  ? "translate(6.000000, 6.000000) rotate(-180.000000) translate(-6.000000, -6.000000)"
                  : ""
              }
            />
          </g>
        </g>
      </g>
    </svg>
  </div>
);
export default Marker;
