import React from "react";
import GoogleMapReact from "google-map-react";
import PropTypes from "prop-types";

const GoogleMap = ({ children, ...props }) => (
  <div className="google-maps-wrapper">
    <GoogleMapReact
      // eslint-disable-next-line no-undef
      bootstrapURLKeys={{ key: process.env.REACT_APP_MAP_KEY }}
      {...props}
    >
      {children}
    </GoogleMapReact>
  </div>
);
GoogleMap.propTypes = {
  children: PropTypes.node.isRequired
};
export default GoogleMap;
