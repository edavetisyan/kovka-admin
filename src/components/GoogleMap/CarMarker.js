import React from "react";
import truck from "../../assets/img/truck.png";

const Marker = () => (
  <div className={`map-marker`}>
    <img src={truck} alt="truck" className="truck-marker-img" />
  </div>
);
export default Marker;
