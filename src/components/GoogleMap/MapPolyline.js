import React, { useState, useEffect, useCallback, useMemo } from "react";
import PropTypes from "prop-types";
import GoogleMap, { Marker } from "./";
import { useFetch } from "../../hooks/useFetch";

let prevPolyline = null;

function MapPolyline({ car, from, to }) {
  const [mapObj, setMapObj] = useState({});
  const response = useFetch(`map/${from}/${to}`);
  useEffect(() => {
    const { map, maps } = mapObj;
    if (response.polyline && map) {
      const routePath = new maps.Polyline({
        path: response.polyline,
        geodesic: true,
        strokeColor: "#003c80",
        strokeOpacity: 1.0,
        strokeWeight: 4
      });
      // prevPolyline = routePath;
      routePath.setMap(map);
    }
  }, [mapObj, response.data.start_location, response.polyline]);
  useEffect(() => {
    const { map, maps } = mapObj;
    if (map) {
      const bounds = new maps.LatLngBounds();

      if (prevPolyline) prevPolyline.setMap(null);
      if (from) bounds.extend(response.data.start_location);
      if (to) bounds.extend(response.data.end_location);
      if (!bounds.isEmpty()) {
        map.fitBounds(bounds);
      }
    }
  }, [
    mapObj,
    from,
    to,
    response.data.end_location,
    response.data.start_location
  ]);

  const mapLoaded = useCallback(data => {
    if (prevPolyline) prevPolyline.setMap(null);
    setMapObj(data);
  }, []);
  const total = useMemo(() => {
    let price = null;
    const mapDistance = response.data.distance.value / 1000;
    if (Number(car.distance) >= mapDistance) {
      price = car.disPrice;
      return price;
    }
    price = Number(car.priceKm) * mapDistance;
    return price;
  }, [car.disPrice, car.distance, car.priceKm, response.data.distance]);
  return (
    <div className="maps-content">
      <div className="map-title">
        <p>
          Distance- {response.data.distance.text}{" "}
          <span>Price- {total} AMD</span>
        </p>
      </div>
      <GoogleMap
        defaultZoom={14}
        //center the map to ARM
        defaultCenter={[40.18062, 44.50818]}
        yesIWantToUseGoogleMapApiInternals
        options={{
          gestureHandling: "greedy",
          minZoom: 1,
          // TODO: set only when bounds
          maxZoom: 16,
          fullscreenControl: false,
          mapTypeControl: false,
          mapTypeControlOptions: {
            style: 1,
            position: 2
          },
          controlSize: 32
        }}
        onGoogleApiLoaded={({ map, maps }) => mapLoaded({ map, maps })}
      >
        {from && (
          <Marker
            lat={response.data.start_location.lat}
            lng={response.data.start_location.lng}
          />
        )}
        {to && (
          <Marker
            pickup={response.data.end_location}
            lat={response.data.end_location.lat}
            lng={response.data.end_location.lng}
          />
        )}
      </GoogleMap>
    </div>
  );
}
MapPolyline.propTypes = {
  to: PropTypes.string.isRequired,
  from: PropTypes.string.isRequired,
  car: PropTypes.objectOf([PropTypes.string, PropTypes.number])
};
export default MapPolyline;
