import React, { Suspense } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { AdminHeader } from "../Header";
import Sidebar from "../Sidebar";
import Modal from "../Modal";
import s from "./layout.module.css";
import Loading from "../Loading";
// import { setParam } from '../../actions/ui';

const Layout = ({ children, pageTitle, breadcrumb, errorModal, setParam }) => (
  <>
    <AdminHeader />
    <div className={s.root}>
      <div className={s.sidebar}>
        <Sidebar />
      </div>
      <div className={s["page-content"]}>
        <div className={s["page-header"]}>
          <div className={s.title}>
            <h1>{pageTitle}</h1>
          </div>
          <ul className={s.breadcrumb}>
            {breadcrumb.map((item, key) => (
              <li key={key}>
                {item.url ? (
                  <Link to={item.url}>
                    <div className={s["breadcrumb-text"]}>
                      <span>{item.name}</span>
                    </div>
                  </Link>
                ) : (
                  <div className={s["breadcrumb-text"]}>
                    <span>{item.name}</span>
                  </div>
                )}
              </li>
            ))}
          </ul>
        </div>
        <Suspense fallback={<Loading />}>
          <div className={s["main-content"]}>{children}</div>
        </Suspense>
        {errorModal && (
          <Modal
            title="Error"
            cancel="OK"
            text={errorModal}
            //   onClose={() => setParam('errorModal', false)}
          />
        )}
      </div>
    </div>
  </>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  pageTitle: PropTypes.string.isRequired,
  breadcrumb: PropTypes.arrayOf(PropTypes.shape({})),
  errorModal: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};

Layout.defaultProps = {
  breadcrumb: []
};

export default Layout;
