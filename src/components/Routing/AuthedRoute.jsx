/* eslint-disable no-unused-vars */
import React, { useContext } from "react";
import PropTypes from "prop-types";
import { Redirect, Route, withRouter } from "react-router-dom";
import { AuthContext } from "../../stores/auth";

function AuthedRoute({
  component: Component,
  permission,
  redirect,
  ...rest
}) {
  const {
    state: { authenticated }
  } = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={props => {
         if (authenticated) {
          return <Component {...props} />;
        }
        return <Redirect to="/" />;

      }}
    />
  );
}

AuthedRoute.propTypes = {
  component: PropTypes.func,
  redirect: PropTypes.bool,
  showLoader: PropTypes.bool,
  user: PropTypes.objectOf(PropTypes.any),
  isUserLoaded: PropTypes.bool,
  authenticated: PropTypes.bool
};

AuthedRoute.defaultProps = {
  redirect: true,
  authenticated: true,
  isUserLoaded: false,
  user:{},
  showLoader: false,
  permission: "admin"
};

export default withRouter(AuthedRoute);
