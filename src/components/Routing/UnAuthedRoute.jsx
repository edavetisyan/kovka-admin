import React, {useContext} from "react";
import PropTypes from "prop-types";
import { Redirect, Route, withRouter } from "react-router-dom";
import { AuthContext } from "../../stores/auth";


const UnAuthedRoute = ({ component: Component, ...rest }) => {
  const {
    state: { authenticated }
  } = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={props => {
        if (!authenticated) {
          return <Component {...props} />;
        } else if (rest.redirect) {
          return <Redirect to="/dashboard" />;
        }
      }}
    />
  );
};

UnAuthedRoute.defaultProps = {
  redirect: true
};

UnAuthedRoute.propTypes = {
  component: PropTypes.func,
  authenticated: PropTypes.bool
};

export default withRouter(UnAuthedRoute);
