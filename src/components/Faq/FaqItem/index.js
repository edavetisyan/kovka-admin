import React, { useState } from "react";
import { Paragraph } from "../../Custom";
import classNames from "classnames";
import PropTypes from "prop-types";
import s from "./item.module.css";

function FaqItem({ question, answer }) {
  const [active, setActive] = useState(false);
  return (
    <div className={s.wrap}>
      <div className={s.question} onClick={() => setActive(!active)}>
        <h5>{question}</h5>
      </div>
      <div className={classNames(s.answer, active ? s.active : {})}>
        <Paragraph>{answer}</Paragraph>
      </div>
    </div>
  );
}

FaqItem.propTypes = {
  question: PropTypes.string,
  answer: PropTypes.string
};
FaqItem.defaultProps = {
  question: "",
  answer: ""
};

export default FaqItem;
