import React from "react";
import PropTypes from "prop-types";
import Container from "../../Custom";
import words from "../../../utils/words";
import s from "./about.module.css";

function About({ lang }) {
  return (
    <section className={s.about} id={words[lang].about.title}>
      <Container>
        <div className={s.title}>
          <h2>{words[lang].about.title}</h2>
        </div>
        <div className={s.text}>
          <p>{words[lang].about.text}</p>
        </div>
      </Container>
    </section>
  );
}

About.propTypes = {
  lang: PropTypes.string
};

About.defaultProps = {
  lang: "hy"
};

export default About;
