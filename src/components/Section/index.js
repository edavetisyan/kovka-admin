import About from './About';
import Order from './Order';
import Delivery from './Delivery';
import Advantages from './Advantages';
import ContactUs from './ContactUs';
export {
  About,
  Order,
  Delivery,
  Advantages,
  ContactUs
}