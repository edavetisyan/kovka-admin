import React from "react";
import { Row, Col } from "react-flexbox-grid";
import PropTypes from "prop-types";
import Container from "../../Custom";
import words from "../../../utils/words";
import s from "./delivery.module.css";

function Delivery({ lang }) {
  return (
    <section id={words[lang].delivery.title}>
      <div className={s.wrap}>
        <Container>
          <div className={s.title}>
            <h2>{words[lang].delivery.title}</h2>
          </div>
          <div className={s.content}>
            <Row>
              {words[lang].delivery.item.map(element => {
                return (
                  <Col sm={3} key={element.id}>
                    <div className={s.item}>
                      <div className={s.img}>
                        <img src={element.img} alt="delivery" />
                      </div>
                      <div className={s.text}>
                        <p>{element.text}</p>
                      </div>
                    </div>
                  </Col>
                );
              })}
            </Row>
          </div>
        </Container>
      </div>
    </section>
  );
}
Delivery.propTypes = {
  lang: PropTypes.string
};

Delivery.defaultProps = {
  lang: "hy"
};
export default Delivery;
