import React, {
  useCallback,
  useReducer,
  useMemo,
  useState,
  Suspense,
  useRef
} from "react";
import s from "./order.module.css";
import { Wrap, Box } from "../../Custom";
import Form, { RadioWrapp, Radio } from "../../Form";
import { useFetch } from "../../../hooks/useFetch";
import { MapPolyline } from "../../GoogleMap";
import ShowError from "../../ShowError";
import Button from "../../Custom/Button";
import Action from "../../../components/Custom/Action";
import Loading from "../../Loading";

const values = {
  from: "",
  to: "",
  car: ""
};

const menu = [
  {
    href: "/sign-in",
    name: "Sign In"
  }
];

function Order() {
  const response = useFetch("car");
  const [showMap, setMap] = useState(false);
  const errorRef = useRef(false);

  const stateReducer = (state, { type, payload }) => {
    switch (type) {
      case "HANDLE_CHANGE":
        return {
          ...state,
          values: { ...state.values, [payload.name]: payload.value }
          // errors: { ...state.errors, [payload.name]: payload.error }
        };
      default:
        return state;
    }
  };
  const [state, dispatchState] = useReducer(stateReducer, {
    values
  });
  const resCar = useMemo(() => {
    const data = response.find(x => x.id === state.values.car);
    return data;
  }, [response, state.values.car]);
  const handleChange = useCallback((name, value) => {
    dispatchState({
      type: "HANDLE_CHANGE",
      payload: { name, value }
    });
  }, []);
  const handleSubmit = useCallback(() => {
    if (!state.values.car || !state.values.from || !state.values.to) {
      // TODO: improve
      setTimeout(() => {
        errorRef.current.show();
      });
      return;
    }
    setMap(true);
  }, [state.values.car, state.values.from, state.values.to]);
  return (
    <section className={s.wrap}>
      <Box>
        <ShowError ref={errorRef} />
        <Wrap>
          <RadioWrapp>
            {response.map(element => {
              return (
                <Radio
                  key={element.id}
                  img={element.avatar}
                  name="car"
                  handleChange={() => handleChange("car", element.id)}
                  value={element.id}
                  checked={state.values.car === element.id}
                  width="50px"
                  height="50px"
                />
              );
            })}
          </RadioWrapp>
          <div className={s.addressWrapp}>
            <Form
              content={[
                {
                  componentType: "address",
                  name: "from",
                  handleChange,
                  placeholder: "From"
                },
                {
                  componentType: "address",

                  name: "to",
                  handleChange,
                  placeholder: "To"
                }
              ]}
            />
            <Button onClick={handleSubmit}>Submit</Button>
          </div>
        </Wrap>
        <Suspense fallback={<Loading />}>
          <div className="orderMap">
            {showMap && (
              <>
                <MapPolyline
                  car={resCar}
                  from={state.values.from.formatted_address}
                  to={state.values.to.formatted_address}
                />
                <Action text="Sign in for an account" menu={menu} />
              </>
            )}
          </div>
        </Suspense>
      </Box>
    </section>
  );
}
export default Order;
