import React from "react";
import { Row, Col } from "react-flexbox-grid";
import PropTypes from "prop-types";
import Container from "../../Custom";
import words from "../../../utils/words";
import s from "./advantages.module.css";

function Advantages({ lang }) {
  return (
    <section id={words[lang].advantages.title}>
      <div className={s.wrap}>
        <Container>
          <div className={s.title}>
            <h2>{words[lang].advantages.title}</h2>
          </div>
          <div className={s.content}>
            <Row>
              {words[lang].advantages.item.map(element => {
                return (
                  <Col sm={3} key={element.id}>
                    <div className={s.item}>
                      <div className={s.img}>
                        <img src={element.img} alt="Advantages" />
                      </div>
                      <div className={s.text}>
                        <p>{element.text}</p>
                      </div>
                    </div>
                  </Col>
                );
              })}
            </Row>
          </div>
        </Container>
      </div>
    </section>
  );
}
Advantages.propTypes = {
  lang: PropTypes.string
};

Advantages.defaultProps = {
  lang: "hy"
};
export default Advantages;
