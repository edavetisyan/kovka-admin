import React from "react";
import Form from "../../Form";
import PropTypes from "prop-types";
import Container from "../../Custom/Container";
import words from "../../../utils/words";
import s from "./contact.module.css";

function ContactUs({ lang }) {
  return (
    <section id={words[lang].contactUs.title}>
      <div className={s.wrap}>
        <Container>
          <div className={s.title}>
            <h2>{words[lang].contactUs.title}</h2>
          </div>
          <Form
            content={words[lang].contactUs.inputs}
            button={{
              type: "submit",
              title: words[lang].contactUs.btn
            }}
            onSubmit={() => false}
            isLoading={false}
          />
        </Container>
      </div>
    </section>
  );
}

ContactUs.propTypes = {
  lang: PropTypes.string
};

ContactUs.defaultProps = {
  lang: "hy"
};
export default ContactUs;
