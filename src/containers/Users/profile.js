import React, { Suspense } from "react";
import PropTypes from "prop-types";
import UserInfo from "../../components/UserInfo";
import Layout from "../../components/Layout";
import Loading from "../../components/Loading";

function Profile({ match }) {
  return (
    <Layout breadcrumb={[]} isLoaded>
      <Suspense fallback={<Loading />}>
        <UserInfo id={match.params.id} />
      </Suspense>
    </Layout>
  );
}
Profile.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  })
};

export default Profile;
