/* eslint-disable react/prop-types */
/* eslint-disable no-undef */
/* eslint-disable react/display-name */
import React, { useState, useEffect, useCallback } from "react";
import ReactTable from "react-table";
import Layout from "../../components/Layout";
import Button from "../../components/Custom/Button";
import { Col, Row } from "react-flexbox-grid";
import { DateTime } from "luxon";
import Modal from "../../components/Modal";
import fetchData from "../../utils/fetchData";
import Loading from "../../components/Loading";
// import defaultImage from "../../assets/img/deliver_icon4.png";

function Driver() {
  const [model, setModel] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [driver, setDriver] = useState([]);
  const [item, setItem] = useState("");

  useEffect(() => {
    setLoading(true);
    fetchData(`${process.env.REACT_APP_API_URL}user/driver`, { method: "GET" })
      .then(res => setDriver(res.data))
      .catch(err => {
        console.log("err", err);
      })
      .finally(setLoading(false));
  }, []);
  const handleOpenModal = useCallback(e => {
    setItem(e);
    setModel(true);
  }, []);
  const handleDelete = useCallback(() => {
    fetchData(`${process.env.REACT_APP_API_URL}user/remove/${item}`, {
      method: "delete"
    })
      .then(() => {
        setModal(false);
      })
      .catch(e => {
        console.log(e);
      });
  }, [item]);
  return (
    <Layout
      pageTitle="Users"
      isLoaded
      breadcrumb={[
        {
          name: "Home",
          url: "/dashboard"
        },
        {
          name: "Driver"
        }
      ]}
    >
      {isLoading ? (
        <Loading />
      ) : (
        <ReactTable
          data={driver}
          defaultPageSize={5}
          filterable
          columns={[
            {
              Header: "First Name",
              accessor: "firstname"
            },
            {
              Header: "Last Name",
              accessor: "lastname"
            },
            {
              Header: "Phone",
              accessor: "phone"
            },
            {
              Header: "Email",
              accessor: "email"
            },
            {
              Header: "Birthday",
              accessor: "birthday"
            },
            {
              Header: "Created At",
              accessor: "createdAt",
              Cell: props =>
                DateTime.fromISO(props.value).toLocaleString(DateTime.DATE_MED)
            },
            {
              Header: "Actions",
              accessor: "id",
              Cell: props => {
                return (
                  <Row end="md">
                    <Col>
                      <Button
                        theme="primary"
                        size="sm"
                        link={{
                          href: `/profile/${props.value}`
                        }}
                      >
                        View
                      </Button>
                    </Col>
                    <Col>
                      <Button
                        theme="danger"
                        size="sm"
                        onClick={() => handleOpenModal(props.value)}
                      >
                        Delete
                      </Button>
                    </Col>
                  </Row>
                );
              }
            }
          ]}
        />
      )}

      {model && (
        <Modal
          title="Delete"
          submitText="Delete"
          text="Are you sure?"
          onSubmit={() => handleDelete(item)}
          onClose={() => setModel(false)}
        />
      )}
    </Layout>
  );
}

export default Driver;
