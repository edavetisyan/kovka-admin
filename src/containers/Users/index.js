/* eslint-disable react/display-name */
import React from "react";
import ReactTable from "react-table";
import Layout from "../../components/Layout";
import Button from "../../components/Custom/Button";
import { Col, Row } from "react-flexbox-grid";
import { DateTime } from "luxon";
import Modal from "../../components/Modal";
// import defaultImage from "../../assets/img/deliver_icon4.png";

function Users({ isOpenDeleteModal }) {
  return (
    <Layout
      pageTitle="Users"
      isLoaded
      breadcrumb={[
        {
          name: "Home",
          url: "/dashboard"
        },
        {
          name: "Users"
        }
      ]}
    >
      <ReactTable
        data={[]}
        defaultPageSize={5}
        filterable
        columns={[
          {
            Header: "First Name",
            accessor: "firstname"
          },
          {
            Header: "Last Name",
            accessor: "lastname"
          },
          {
            Header: "Phone",
            accessor: "phone"
          },
          {
            Header: "Email",
            accessor: "email"
          },
          {
            Header: "Birthday",
            accessor: "birthday"
          },
          {
            Header: "Created At",
            accessor: "createdAt",
            Cell: props =>
              DateTime.fromISO(props.value).toLocaleString(DateTime.DATE_MED)
          },
          {
            Header: "Actions",
            accessor: "id",
            Cell: props => {
              return (
                <Row end="md">
                  <Col>
                    <Button
                      theme="success"
                      size="sm"
                      link={{
                        href: `/cars/edit/${props.value}`
                      }}
                    >
                      Edit
                    </Button>
                  </Col>
                  <Col>
                    <Button theme="danger" size="sm" onClick={() => false}>
                      Delete
                    </Button>
                  </Col>
                </Row>
              );
            }
          }
        ]}
      />
      {/* <Row end="md">
        <Button
          link={{
            href: "cars/create"
          }}
        >
          Add
        </Button>
      </Row> */}
      {isOpenDeleteModal && (
        <Modal
          title="Delete"
          submitText="Delete"
          text="Are you sure?"
          onSubmit={() => this.handleDelete(isOpenDeleteModal)}
          onClose={() => this.props.setParam("isOpenDeleteModal", false)}
        />
      )}
    </Layout>
  );
}

export default Users;
