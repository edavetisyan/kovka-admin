/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";
import Layout from "../../components/Layout";
import Container from "../../components/Custom/Container";
import Form from "../../components/Form";
import fetchData from "../../utils/fetchData";

function CreateCategory({ history }) {
  const [isLoading, setLoading] = useState(false);

  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      fetchData(`${process.env.REACT_APP_API_URL}categories/create`, {
        body: data
      })
        .then(() => {
          history.push("/category");
        })
        .catch(e => {
        })
        .finally(() => setLoading(false));
    },
    [history]
  );
  return (
    <div>
      <Layout
        pageTitle="Создать категорию"
        breadcrumb={[
          {
            name: "Категория",
            url: "/category"
          },
          {
            name: "Создать категорию"
          }
        ]}
        isLoaded
      >
        <Container>
          <Form
            rowCenter="center"
            content={[
              {
                componentType: "input",
                name: "name",
                label: "Категория *",
                size: 6,
                isRequired: true
              },
              {
                componentType: "input",
                name: "slug",
                label: "Slug *",
                size: 6,
                isRequired: true
              }
            ]}
            button={{
              type: "submit",
              title: "submit"
            }}
            onSubmit={handleSubmit}
            isLoading={isLoading}
          />
        </Container>
      </Layout>
    </div>
  );
}
CreateCategory.propTypes = {
  history: PropTypes.shape({ push: PropTypes.func }).isRequired
};
export default CreateCategory;
