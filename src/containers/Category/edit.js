/* eslint-disable no-undef */
import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";
import Form from "../../components/Form";
import Layout from "../../components/Layout";
import Container from "../../components/Custom/Container";
import fetchData from "../../utils/fetchData";
import { useFetch } from "../../hooks/useFetch";

function EditCategory({ match, history }) {
  const data = useFetch(`categories/${match.params.id}`);
  const [isLoading, setLoading] = useState(false);

  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      data.id = match.params.id;
      fetchData(`${process.env.REACT_APP_API_URL}categories/update`, {
        body: data,
        method: "PUT"
      })
        .then(() => {
          history.push("/category");
        })
        .catch(e => {
        })
        .finally(() => setLoading(false));
    },
    [history, match.params.id]
  );

  return (
    <Layout
      pageTitle="Create Size"
      breadcrumb={[
        {
          name: "categories",
          url: "/categories"
        },
        {
          name: "Create categories"
        }
      ]}
      isLoaded
    >
      <Container>
        <Form
          rowCenter="center"
          content={[
            {
              componentType: "input",
              name: "name",
              label: "Categories *",
              initialValue: data.name,
              size: 5,
              isRequired: true
            },
            {
              componentType: "input",
              name: "slug",
              label: "Slug *",
              initialValue: data.slug,
              size: 5,
              isRequired: true
            }
          ]}
          button={{
            type: "submit",
            title: "submit"
          }}
          onSubmit={handleSubmit}
          isLoading={isLoading}
        />
      </Container>
    </Layout>
  );
}
EditCategory.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  })
};

export default EditCategory;
