/* eslint-disable no-undef */
/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React, { useState, useCallback, useEffect } from "react";
import ReactTable from "react-table";
import Layout from "../../components/Layout";
import Button from "../../components/Custom/Button";
import { Col, Row } from "react-flexbox-grid";
// import { DateTime } from "luxon";
import Modal from "../../components/Modal";
import fetchData from "../../utils/fetchData";
import Loading from "../../components/Loading";

function Size() {
  const [isLoading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [sizes, setSizes] = useState([]);
  const [item, setItem] = useState("");
  useEffect(() => {
    setLoading(true);
    fetchData(`${process.env.REACT_APP_API_URL}categories`, { method: "GET" })
      .then(res => setSizes(res.data))
      .catch(err => {
      })
      .finally(setLoading(false));
  }, []);
  const handleOpenModal = useCallback(e => {
    setItem(e);
    setModal(true);
  }, []);
  const handleDelete = useCallback(() => {
    fetchData(`${process.env.REACT_APP_API_URL}categories/remove/${item}`, {
      method: "delete"
    })
      .then(() => {
        setModal(false);
      })
      .catch(e => {
        console.log(e);
      });
  }, [item]);
  return (
    <Layout
      pageTitle="Категория"
      isLoaded
      breadcrumb={[
        {
          name: "Home",
          url: "/dashboard"
        },
        {
          name: "Категория"
        }
      ]}
    >
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <ReactTable
            data={sizes}
            defaultPageSize={5}
            filterable
            columns={[
              {
                Header: "Категория",
                accessor: "name"
              },
              {
                Header: "Slug",
                accessor: "slug"
              },
              {
                Header: "Actions",
                accessor: "id",
                Cell: props => {
                  return (
                    <Row end="md">
                      <Col>
                        <Button
                          theme="success"
                          size="sm"
                          link={{
                            href: `/category/edit/${props.value}`
                          }}
                        >
                          Edit
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          theme="danger"
                          size="sm"
                          onClick={() => handleOpenModal(props.value)}
                        >
                          Delete
                        </Button>
                      </Col>
                    </Row>
                  );
                }
              }
            ]}
          />
          <Row end="md">
            <Button
              link={{
                href: "category/create"
              }}
            >
              Add
            </Button>
          </Row>
        </>
      )}
      {modal && (
        <Modal
          title="Delete"
          submitText="Delete"
          text="Are you sure?"
          onSubmit={() => handleDelete()}
          onClose={() => setModal(false)}
        />
      )}
    </Layout>
  );
}

export default Size;
