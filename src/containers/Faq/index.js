import React from "react";
import Container, { Wrap, Box, Title } from "../../components/Custom";
import { FaqItem } from "../../components/Faq";
import Header from "../../components/Header";
import Footer from "../../components/Footer";

const data = [
  {
    id: 1,
    question: "Why do we use it?",
    answer:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  },
  {
    id: 2,
    question: "What is Lorem Ipsum?",
    answer:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English"
  },
  {
    id: 3,
    question: "Where does it come from?",
    answer:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English."
  },
  {
    id: 4,
    question: "Where can I get some?",
    answer:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English."
  }
];

function Faq() {
  return (
    <>
      <div className="wrap">
        <Header />
        <div className="content">
          <Container>
            <Wrap>
              <Box>
                <Title title="Faq" />
                {data.map(element => {
                  return (
                    <FaqItem
                      key={element.id}
                      answer={element.answer}
                      question={element.question}
                    />
                  );
                })}
              </Box>
            </Wrap>
          </Container>
        </div>
        <Footer />
      </div>
    </>
  );
}
export default Faq;
