import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import {
  Order,
  About,
  Delivery,
  Advantages,
  ContactUs
} from "../../components/Section";
function Homepage() {
  return (
    <>
      <Header />
      <Order />
      <About />
      <Delivery />
      <Advantages />
      <ContactUs />
      <Footer />
    </>
  );
}
export default Homepage;
