import React, { useContext, useCallback, useState } from "react";
import PropTypes from "prop-types";


import storage from "../../utils/storage";
import { Wrap, Box, Title } from "../../components/Custom";
import { SettingsContext } from "../../stores/settings";
import { AuthContext } from "../../stores/auth";
import { SET_USER } from "../../constants/auth";

import Form from "../../components/Form";
import { Signin } from "../../utils/lang";
import fetchData from "../../utils/fetchData";

function SignIn({ history }) {
  const {
    state: { language }
  } = useContext(SettingsContext);
  const [isLoading, setLoading] = useState(false);
  const { dispatch } = useContext(AuthContext);
  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      // eslint-disable-next-line no-undef
      fetchData(`${process.env.REACT_APP_API_URL}auth/admin`, { body: data })
        .then(res => {
          dispatch({
            type: SET_USER,
            payload: res.data
          });
          storage.set("token", res.data.token);
          history.push("/dashboard");
        })
        .catch(e => {
          // eslint-disable-next-line no-undef
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    },
    [dispatch, history]
  );
  return (
    <div className="wrap">
      <div className="content">
        <Title title="Sign In" />
        <Wrap>
          <Box>
            <Form
              content={Signin[language].inputs}
              button={{
                type: "submit",
                title: Signin[language].btn
              }}
              onSubmit={handleSubmit}
              isLoading={isLoading}
            />
          </Box>
        </Wrap>
      </div>
    </div>
  );
}
SignIn.propTypes = {
  history: PropTypes.shape({ push: PropTypes.func }).isRequired
};
export default SignIn;
