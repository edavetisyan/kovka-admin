/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import React, { useCallback, useState, useMemo } from "react";
import PropTypes from "prop-types";
import Layout from "../../components/Layout";
import Container from "../../components/Custom/Container";
import Form from "../../components/Form";
import fetchData from "../../utils/fetchData";
import { useFetch } from "../../hooks/useFetch";

function SubCategoryCreate({ history }) {
  const [isLoading, setLoading] = useState(false);
  const result = useFetch("categories");
  const sub = useMemo(() => {
    const res = [];
    result.forEach(value => {
      res.push({
        label: value.name,
        value: value.id
      });
    });
    return res;
  }, [result]);

  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      fetchData(`${process.env.REACT_APP_API_URL}subCategories/create`, {
        body: data
      })
        .then(() => {
          history.push("/category");
        })
        .catch(e => {
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    },
    [history]
  );
  return (
    <div>
      <Layout
        pageTitle="Создать подкатегория"
        breadcrumb={[
          {
            name: "SubCategory",
            url: "/sub_category"
          },
          {
            name: "Создать подкатегория"
          }
        ]}
        isLoaded
      >
        <Container>
          <Form
            rowCenter="center"
            content={[
              {
                componentType: "select",
                name: "catId",
                label: "Cateogries *",
                size: 6,
                options: [...sub],
                isRequired: true
              },
              {
                componentType: "input",
                name: "name",
                label: "Подкатегория *",
                size: 6,
                isRequired: true
              },
              {
                componentType: "input",
                name: "slug",
                label: "Slug *",
                size: 6,
                isRequired: true
              }
            ]}
            button={{
              type: "submit",
              title: "submit"
            }}
            onSubmit={handleSubmit}
            isLoading={isLoading}
          />
        </Container>
      </Layout>
    </div>
  );
}
SubCategoryCreate.propTypes = {
  history: PropTypes.shape({ push: PropTypes.func }).isRequired
};
export default SubCategoryCreate;
