/* eslint-disable no-undef */
import React, { useCallback, useState, useMemo } from "react";
import PropTypes from "prop-types";
import Form from "../../components/Form";
import Layout from "../../components/Layout";
import Container from "../../components/Custom/Container";
import fetchData from "../../utils/fetchData";
import { useFetch } from "../../hooks/useFetch";

function SubCategoryEdit({ match, history }) {
  const result = useFetch("categories");
  const data = useFetch(`subCategories/${match.params.id}`);
  const [isLoading, setLoading] = useState(false);
  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      data.id = match.params.id;
      fetchData(`${process.env.REACT_APP_API_URL}subCategories/update`, {
        body: data,
        method: "PUT"
      })
        .then(() => {
          history.push("/sub-category");
        })
        .catch(e => {
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    },
    [history, match.params.id]
  );
  const selectedCat = useMemo(()=>{
    const res = result.find(x => x.id === data.catId);
    if(res){
      return(
        [
          {
            label:res.name,
            value: res.id
          }
        ]
      )
    }

  },[result, data.catId])
  const sub = useMemo(() => {
    const res = [];
    result.forEach(value => {
      res.push({
        label: value.name,
        value: value.id
      });
    });
    return res;
  }, [result]);
  return (
    <Layout
      pageTitle="Create SubCategory"
      breadcrumb={[
        {
          name: "SubCategory",
          url: "/sub-category"
        },
        {
          name: "Create SubCategory"
        }
      ]}
      isLoaded
    >
      <Container>
        <Form
          rowCenter="center"
          content={[
            {
              componentType: "select",
              name: "catId",
              label: "Cateogries *",
              size: 6,
              initialValue: selectedCat[0].value,
              options: [...sub],
              isRequired: true
            },
            {
              componentType: "input",
              name: "name",
              label: "Подкатегория *",
              initialValue: data.name,
              size: 6,
              isRequired: true
            },
            {
              componentType: "input",
              name: "slug",
              label: "Slug *",
              initialValue: data.slug,
              size: 6,
              isRequired: true
            }
          ]}
          button={{
            type: "submit",
            title: "submit"
          }}
          onSubmit={handleSubmit}
          isLoading={isLoading}
        />
      </Container>
    </Layout>
  );
}
SubCategoryEdit.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  })
};

export default SubCategoryEdit;
