/* eslint-disable no-undef */
/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React, { useState, useCallback, useEffect } from "react";
import ReactTable from "react-table";
import Layout from "../../components/Layout";
import Button from "../../components/Custom/Button";
import { Col, Row } from "react-flexbox-grid";
// import { DateTime } from "luxon";
import Modal from "../../components/Modal";
import fetchData from "../../utils/fetchData";
import Loading from "../../components/Loading";

function SubCategory() {
  const [isLoading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [sabCat, setSabCat] = useState([]);
  const [item, setItem] = useState("");
  useEffect(() => {
    setLoading(true);
    fetchData(`${process.env.REACT_APP_API_URL}subCategories`, { method: "GET" })
      .then(res => setSabCat(res.data))
      .catch(err => {
        console.log("err", err);
      })
      .finally(setLoading(false));
  }, []);
  const handleOpenModal = useCallback(e => {
    setItem(e);
    setModal(true);
  }, []);
  const handleDelete = useCallback(() => {
    fetchData(`${process.env.REACT_APP_API_URL}subCategories/remove/${item}`, {
      method: "delete"
    })
      .then(() => {
        setModal(false);
      })
      .catch(e => {
        console.log(e);
      });
  }, [item]);
  return (
    <Layout
      pageTitle="Подкатегория"
      isLoaded
      breadcrumb={[
        {
          name: "Home",
          url: "/dashboard"
        },
        {
          name: "Подкатегория"
        }
      ]}
    >
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <ReactTable
            data={sabCat}
            defaultPageSize={5}
            filterable
            columns={[
              {
                Header: "Подкатегория",
                accessor: "name"
              },
              {
                Header: "Slug",
                accessor: "slug"
              },
              {
                Header: "Category",
                accessor: "category.name"
              },
              {
                Header: "Actions",
                accessor: "id",
                Cell: props => {
                  return (
                    <Row end="md">
                      <Col>
                        <Button
                          theme="success"
                          size="sm"
                          link={{
                            href: `/sub-category/edit/${props.value}`
                          }}
                        >
                          Edit
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          theme="danger"
                          size="sm"
                          onClick={() => handleOpenModal(props.value)}
                        >
                          Delete
                        </Button>
                      </Col>
                    </Row>
                  );
                }
              }
            ]}
          />
          <Row end="md">
            <Button
              link={{
                href: "sub-category/create"
              }}
            >
              Add
            </Button>
          </Row>
        </>
      )}
      {modal && (
        <Modal
          title="Delete"
          submitText="Delete"
          text="Are you sure?"
          onSubmit={() => handleDelete()}
          onClose={() => setModal(false)}
        />
      )}
    </Layout>
  );
}

export default SubCategory;
