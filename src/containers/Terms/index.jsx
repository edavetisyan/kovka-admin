import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Container, {
  Wrap,
  Box,
  Title,
  Paragraph
} from "../../components/Custom";

function Terms() {
  return (
    <div className="wrap">
      <Header />
      <div className="content">
        <Title title="Terms" />
        <Container>
          <Wrap>
            <Box>
              <Paragraph center>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry s standard dummy
                text <br />
                ever since the 1500s, when an unknown printer took a galley
              </Paragraph>
              <Paragraph center>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
              </Paragraph>
              <Paragraph center>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
              </Paragraph>
              <Paragraph center>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
              </Paragraph>
            </Box>
          </Wrap>
        </Container>
      </div>
      <Footer />
    </div>
  );
}
export default Terms;
