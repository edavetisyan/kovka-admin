import React from "react";
import Layout from "../../components/Layout";
import Title from "../../components/Custom/Title";

function Dashboard() {
  return (
    <div>
      <Layout pageTitle="Dashboard" breadcrumb={[]} isLoaded>
        <Title title="Добро пожаловать в Kovka Kimki" />
      </Layout>
    </div>
  );
}
export default Dashboard;
