/* eslint-disable no-undef */
import React, { useCallback, useState } from "react";
import Form from "../../components/Form";
import PropTypes from "prop-types";
import Layout from "../../components/Layout";
import Container from "../../components/Custom/Container";
import fetchData from "../../utils/fetchData";
import ImageUpload from "../../components/Form/ImageUpload";

function CreateReviews({ history }) {
  // const result = useFetch("custom/sizes");
  const [image, setImage] = useState([]);
  const [isLoading, setLoading] = useState(false)
  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      data.avatar = image;
      fetchData(`${process.env.REACT_APP_API_URL}reviews/create`, { body: data })
        .then(() => {
          history.push("/reviews");
        })
        .catch(e => {
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    },
    [history, image]
  );
  const handleImage = useCallback(e => {
    setImage(e);
  }, []);

  return (
    <Layout
      pageTitle="Отзывы"
      isLoaded
      breadcrumb={[
        {
          name: "Отзывы",
          url: "/reviews"
        },
        {
          name: "Создать"
        }
      ]}
    >
      <Container>
        <ImageUpload handleChagne={handleImage} />
        <Form
          content={[
            {
              componentType: "input",
              name: "name",
              label: "Имя *",
              size: 6,
              isRequired: true
            },
            {
              componentType: "textarea",
              name: "description",
              label: "Описание *",
              size: 12,
              isRequired: true
            },
          ]}
          button={{
            type: "submit",
            title: "submit"
          }}
          onSubmit={handleSubmit}
          isLoading={isLoading}
        />
      </Container>
    </Layout>
  );
}

CreateReviews.propTypes = {
  history: PropTypes.shape({ push: PropTypes.func }).isRequired
};
export default CreateReviews;
