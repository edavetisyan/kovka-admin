import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Container, { Box } from "../../components/Custom";
import s from "./thankYou.module.css";
import email from "../../assets/img/email.png";

function ThankYou() {
  return (
    <div className="wrap">
      <Header />
      <div className="content">
        <Container>
          <div className={s.wrap}>
            <Box>
              <div className={s.img}>
                <img src={email} alt="email" />
              </div>
              <div className={s.text}>
                <h5>Thank You!</h5>
                <p>
                  You have successfully registered in. We sent a mail
                  confirmation letter
                </p>
              </div>
            </Box>
          </div>
        </Container>
      </div>
      <Footer />
    </div>
  );
}
export default ThankYou;
