/* eslint-disable no-undef */
import React, { useContext, useState, useCallback } from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import history from "../../utils/history";
import { Wrap, Box, Title, Action } from "../../components/Custom";
import { SettingsContext } from "../../stores/settings";
import Form from "../../components/Form";
import { SignUp } from "../../utils/lang";
import { CLIENT } from "../../constants/user";
import fetchData from "../../utils/fetchData";

const menu = [
  {
    href: "/sign-in",
    name: "Sign In"
  }
];

function Sign() {
  const {
    state: { language }
  } = useContext(SettingsContext);
  const [isLoading, setLoading] = useState(false);
  const handleSubmit = useCallback(data => {
    data.role = CLIENT;
    delete data.confirmPassword;
    setLoading(true);
    fetchData(`${process.env.REACT_APP_API_URL}auth/signup`, { body: data })
      .then(() => {
        history.push("thank-you");
      })
      .catch(err => {
        console.log("err", err);
        // Alert.alert("Error", "Something went wrong", [
        //   {
        //     text: "ОК"
        //   }
        // ]);
      })
      .finally(() => setLoading(false));
  }, []);
  return (
    <div className="wrap">
      <Header />
      <div className="content">
        <Title title="Sign Up" />
        <Wrap>
          <Box>
            <Form
              content={SignUp[language].client.inputs}
              button={{
                type: "submit",
                title: SignUp[language].btn
              }}
              onSubmit={handleSubmit}
              isLoading={isLoading}
            />
            <Action text="Sign in for an account" menu={menu} />
          </Box>
        </Wrap>
      </div>
      <Footer />
    </div>
  );
}
export default Sign;
