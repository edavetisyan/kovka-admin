import React, { useEffect, useState, useContext } from "react";
import PropTypes from "prop-types";
import io from "socket.io-client";
import _ from "lodash";
import GoogleMap, { CarMarker } from "../../components/GoogleMap";
import { useFetch } from "../../hooks/useFetch";
import Container from "../../components/Custom";
import Layout from "../../components/Layout";
import { AuthContext } from "../../stores/auth";

let socket = null;

const ViewOrder = ({ match }) => {
  const {
    state: { user }
  } = useContext(AuthContext);
  const order = useFetch(`order/id/${match.params.id}`);
  const [coords, setCoords] = useState([]);
  useEffect(() => {
    if (order) {
      socket = io.connect(process.env.REACT_APP_SOCKET_URL, {
        reconnection: true,
        reconnectionDelay: 5000,
        reconnectionDelayMax: 5000,
        reconnectionAttempts: Infinity,
        transports: ["websocket"]
      });

      socket.on("connect", function () {
        socket.emit("join", {
          userId: user.id,
          orderId: order.id,
          role: "client"
        });

        socket.on("position", data => {
          setCoords(data);
        });
      });
    }

    return () => {
      if (socket) {
        socket.disconnect();
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [order]);

  return (
      <Layout
        pageTitle="View Order"
        breadcrumb={[
          {
            name: "Order",
            url: "/order"
          },
          {
            name: "View Order"
          }
        ]}
        isLoaded
      >
        <Container>
          <div className="google-maps-wrapper order-view-map">
            <GoogleMap
              defaultZoom={14}
              //center the map to ARM
              defaultCenter={[40.18062, 44.50818]}
              yesIWantToUseGoogleMapApiInternals
              options={{
                gestureHandling: "greedy",
                minZoom: 1,
                // TODO: set only when bounds
                maxZoom: 16,
                fullscreenControl: false,
                mapTypeControl: false,
                mapTypeControlOptions: {
                  style: 1,
                  position: 2
                },
                controlSize: 32
              }}
            >
              {!_.isEmpty(coords) && (
                <CarMarker lat={coords[0]} lng={coords[1]} />
              )}
            </GoogleMap>
          </div>
        </Container>
      </Layout>
  );
};
ViewOrder.propTypes = {
  children: PropTypes.node.isRequired
};
export default ViewOrder;
