/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import React, { useCallback, useState, useReducer, useMemo } from "react";
import Layout from "../../components/Layout";
import PropTypes from "prop-types";
import Container from "../../components/Custom/Container";
import Form, { Radio, RadioWrapp } from "../../components/Form";
import { Row } from "react-flexbox-grid";
import { MapPolyline } from "../../components/GoogleMap";
import Button from "../../components/Custom/Button";
import fetchData from "../../utils/fetchData";
import { useFetch } from "../../hooks/useFetch";
import Modal from "../../components/Modal";

const values = {
  from: "",
  to: "",
  carId: ""
};
const stateReducer = (state, { type, payload }) => {
  switch (type) {
    case "HANDLE_CHANGE":
      return {
        ...state,
        values: { ...state.values, [payload.name]: payload.value }
        // errors: { ...state.errors, [payload.name]: payload.error }
      };
    default:
      return state;
  }
};
function CreateOrder({ history }) {
  const response = useFetch("car");
  const [isLoading, setLoading] = useState(false);
  const [isModal, setModal] = useState(false);

  const [state, dispatchState] = useReducer(stateReducer, {
    values
  });

  const handleChange = useCallback((name, value) => {
    dispatchState({
      type: "HANDLE_CHANGE",
      payload: { name, value }
    });
  }, []);
  const car = useMemo(() => {
    const data = response.find(x => x.id === state.values.carId);
    return data;
  }, [response, state.values.carId]);
  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      data.carId = state.values.carId;
      fetchData(`${process.env.REACT_APP_API_URL}order/create`, { body: data })
        .then(() => {
          history.push("/order");
        })
        .catch(e => {
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    },
    [history, state.values.carId]
  );
  return (
    <div>
      <Layout
        pageTitle="Create Order"
        breadcrumb={[
          {
            name: "Order",
            url: "/order"
          },
          {
            name: "Create Order"
          }
        ]}
        isLoaded
      >
        <Container>
          {!!state.values.from && !!state.values.to && (
            <Row end="md">
              <Button onClick={() => setModal(true)}>Show Map</Button>
            </Row>
          )}
          <RadioWrapp>
            {response.map(element => {
              return (
                <Radio
                  key={element.id}
                  img={element.avatar}
                  name="carId"
                  handleChange={() => handleChange("carId", element.id)}
                  value={element.id}
                  checked={state.values.carId === element.id}
                  size={element.size.size}
                  width="200px"
                  height="100px"
                />
              );
            })}
          </RadioWrapp>

          <Form
            content={[
              {
                componentType: "address",
                name: "from",
                label: "From *",
                size: 6,
                handleChange,
                isRequired: true
              },
              {
                componentType: "address",
                name: "to",
                label: "To *",
                handleChange,
                size: 6,
                isRequired: true
              },
              {
                componentType: "date",
                name: "date",
                label: "Date *",
                size: 12,
                isRequired: true
              },
              {
                componentType: "textarea",
                name: "notes",
                label: "Notes *",
                size: 12,
                isRequired: true
              }
            ]}
            button={{
              type: "submit",
              title: "submit"
            }}
            onSubmit={handleSubmit}
            isLoading={isLoading}
          />
          {isModal && (
            <Modal
              title="Map"
              text="Distance Map"
              cancel="Close"
              onClose={() => setModal(false)}
            >
              <MapPolyline
                from={state.values.from.formatted_address}
                car={car}
                to={state.values.to.formatted_address}
              />
            </Modal>
          )}
        </Container>
      </Layout>
    </div>
  );
}

CreateOrder.propTypes = {
  history: PropTypes.shape({ push: PropTypes.func }).isRequired
};
export default CreateOrder;
