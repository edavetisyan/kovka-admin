/* eslint-disable no-undef */
/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React, { useState, useCallback, useEffect } from "react";
import ReactTable from "react-table";
import Layout from "../../components/Layout";
import { Button, Status } from "../../components/Custom";
import { Col, Row } from "react-flexbox-grid";
import { DateTime } from "luxon";
import Modal from "../../components/Modal";
import fetchData from "../../utils/fetchData";
import Loading from "../../components/Loading";
import { ACTIVE, DELIVERED, COMPLETED } from "../../constants/order";

function Order() {
  const [isLoading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [order, setOrder] = useState([]);
  const [item, setItem] = useState("");
  useEffect(() => {
    setLoading(true);
    fetchData(`${process.env.REACT_APP_API_URL}order`, { method: "GET" })
      .then(res => setOrder(res.data))
      .catch(err => {
        console.log("err", err);
      })
      .finally(setLoading(false));
  }, []);
  const handleOpenModal = useCallback(e => {
    setItem(e);
    setModal(true);
  }, []);
  const handleDelete = useCallback(() => {
    fetchData(`${process.env.REACT_APP_API_URL}order/remove/${item}`, {
      method: "delete"
    })
      .then(() => {
        setModal(false);
      })
      .catch(e => {
        console.log(e);
      });
  }, [item]);
  return (
    <Layout
      pageTitle="Order"
      isLoaded
      breadcrumb={[
        {
          name: "Home",
          url: "/dashboard"
        },
        {
          name: "Order"
        }
      ]}
    >
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <ReactTable
            data={order}
            defaultPageSize={5}
            filterable
            columns={[
              {
                Header: "From",
                accessor: "from"
              },
              {
                Header: "To",
                accessor: "to"
              },
              {
                Header: "Date",
                accessor: "date"
              },
              {
                Header: "Status",
                accessor: "status",
                Cell: props => {
                  let res = "";
                  if (props.value === ACTIVE) {
                    res = <Status theme="danger">Active</Status>;
                  } else if (props.value === DELIVERED) {
                    res = <Status theme="primary">DELIVERING</Status>;
                  } else if (props.value === COMPLETED) {
                    res = <Status theme="success">COMPLETED</Status>;
                  }
                  return res;
                }
              },
              {
                Header: "Notes",
                accessor: "notes"
              },
              {
                Header: "Created At",
                accessor: "createdAt",
                Cell: props =>
                  DateTime.fromISO(props.value).toLocaleString(
                    DateTime.DATE_MED
                  )
              },
              {
                Header: "Actions",
                accessor: "id",
                Cell: props => {
                  if (props.original.status === ACTIVE) {
                    return (
                      <Row end="md">
                        <Col>
                          <Button
                            theme="success"
                            size="sm"
                            link={{
                              href: `/order/edit/${props.value}`
                            }}
                          >
                            Edit
                          </Button>
                        </Col>
                        <Col>
                          <Button
                            theme="danger"
                            size="sm"
                            onClick={() => handleOpenModal(props.value)}
                          >
                            Delete
                          </Button>
                        </Col>
                      </Row>
                    );
                  }
                  return (
                    <Button
                      theme="primary"
                      size="sm"
                      link={{
                        href: `/order/view/${props.value}`
                      }}
                    >
                      View
                    </Button>
                  );
                }
              }
            ]}
          />
          <Row end="md">
            <Button
              link={{
                href: "order/create"
              }}
            >
              Add
            </Button>
          </Row>
          {modal && (
            <Modal
              title="Delete"
              submitText="Delete"
              text="Are you sure?"
              onSubmit={() => handleDelete(item)}
              onClose={() => setModal(false)}
            />
          )}
        </>
      )}
    </Layout>
  );
}

export default Order;
