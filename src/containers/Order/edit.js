/* eslint-disable no-undef */
import React, {
  useCallback,
  useState,
  useReducer,
  useEffect,
  useMemo
} from "react";
import PropTypes from "prop-types";
import Layout from "../../components/Layout";
import Container from "../../components/Custom/Container";
import fetchData from "../../utils/fetchData";
import { useFetch } from "../../hooks/useFetch";
import Form, { Radio, RadioWrapp } from "../../components/Form";
import Button from "../../components/Custom/Button";
import { Row } from "react-flexbox-grid";
import Modal from "../../components/Modal";
import { MapPolyline } from "../../components/GoogleMap";

const values = {};
const stateReducer = (state, { type, payload }) => {
  switch (type) {
    case "HANDLE_CHANGE":
      return {
        ...state,
        values: { ...state.values, [payload.name]: payload.value }
        // errors: { ...state.errors, [payload.name]: payload.error }
      };
    case "UPDATE_CHANGE":
      return {
        ...state,
        values: { ...state.values, ...payload }
        // errors: { ...state.errors, [payload.name]: payload.error }
      };
    default:
      return state;
  }
};
function EditOrder({ match, history }) {
  const response = useFetch("car");
  const order = useFetch(`order/id/${match.params.id}`);
  const [state, dispatch] = useReducer(stateReducer, {
    values
  });

  useEffect(() => {
    if (
      Object.entries(state.values).length === 0 &&
      state.values.constructor === Object
    ) {
      dispatch({
        type: "UPDATE_CHANGE",
        payload: {
          from: order.from,
          to: order.to,
          car: order.carId
        }
      });
    }
  }, [order.car, order.from, order.to, state.values, state.values.from]);

  const [isLoading, setLoading] = useState(false);
  const [isModal, setModal] = useState(false);

  const car = useMemo(() => {
    const data = response.find(x => x.id === state.values.car);
    return data;
  }, [response, state.values.car]);

  const handleChange = useCallback((name, value) => {
    dispatch({
      type: "HANDLE_CHANGE",
      payload: { name, value }
    });
  }, []);
  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      data.id = match.params.id;
      fetchData(`${process.env.REACT_APP_API_URL}custom/order/update`, {
        body: data,
        method: "PUT"
      })
        .then(() => {
          history.push("/order");
        })
        .catch(e => {
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    },
    [history, match.params.id]
  );

  console.log('state.values.car', state.values.car, order);
  return (
    <Layout
      pageTitle="Create Order"
      breadcrumb={[
        {
          name: "Order",
          url: "/order"
        },
        {
          name: "Create Order"
        }
      ]}
      isLoaded
    >
      <Container>
        {!!state.values.from && !!state.values.to && (
          <Row end="md">
            <Button onClick={() => setModal(true)}>Show Map</Button>
          </Row>
        )}
        <RadioWrapp>
          {response.map(element => {
            return (
              <Radio
                key={element.id}
                img={element.avatar}
                name="car"
                handleChange={() => handleChange("car", element.id)}
                value={element.id}
                checked={state.values.car === element.id}
                size={element.size.size}
                width="200px"
                height="100px"
              />
            );
          })}
        </RadioWrapp>

        <Form
          content={[
            {
              componentType: "address",
              name: "from",
              label: "From *",
              size: 6,
              handleChange,
              initialValue: order.from,
              isRequired: true
            },
            {
              componentType: "address",
              name: "to",
              label: "To *",
              handleChange,
              size: 6,
              initialValue: order.to,
              isRequired: true
            },
            {
              componentType: "date",
              name: "date",
              label: "Date *",
              size: 12,
              initialValue: new Date(order.date),
              isRequired: true
            },
            {
              componentType: "textarea",
              name: "notes",
              label: "Notes *",
              initialValue: order.notes,
              size: 12,
              isRequired: true
            }
          ]}
          button={{
            type: "submit",
            title: "submit"
          }}
          onSubmit={handleSubmit}
          isLoading={isLoading}
        />
        {isModal && (
          <Modal
            title="Map"
            text="Distance Map"
            cancel="Close"
            onClose={() => setModal(false)}
          >
            <MapPolyline
              from={state.values.from}
              car={car}
              to={state.values.to}
            />
          </Modal>
        )}
      </Container>
    </Layout>
  );
}
EditOrder.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  })
};

export default EditOrder;
