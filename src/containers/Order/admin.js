/* eslint-disable no-undef */
/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React, {
  useState,
  useReducer,
  useEffect,
  useCallback,
  Suspense
} from "react";
import ReactTable from "react-table";
import Layout from "../../components/Layout";
import { Button, Status } from "../../components/Custom";
import { Col, Row } from "react-flexbox-grid";
import { DateTime } from "luxon";
import Modal from "../../components/Modal";
import { MapPolyline } from "../../components/GoogleMap";
import fetchData from "../../utils/fetchData";
import Loading from "../../components/Loading";
import { ACTIVE, DELIVERED, COMPLETED } from "../../constants/order";


const stateReducer = (state, { type, payload }) => {
  switch (type) {
    case "HANDLE_CHANGE":
      return {
        ...state,
        values: { ...state.values, ...payload }
        // errors: { ...state.errors, [payload.name]: payload.error }
      };
    default:
      return state;
  }
};

function OrderAll() {
  const [isLoading, setLoading] = useState(false);
  const [isModal, setModal] = useState(false);
  const [order, setOrder] = useState([]);
  const [state, dispatch] = useReducer(stateReducer, {
    values
  });
  useEffect(() => {
    setLoading(true);
    fetchData(`${process.env.REACT_APP_API_URL}order/all`, { method: "GET" })
      .then(res => setOrder(res.data))
      .catch(err => {
        console.log("err", err);
      })
      .finally(setLoading(false));
  }, []);
  const handleChange = useCallback(data => {
    dispatch({
      type: "HANDLE_CHANGE",
      payload: data
    });
    setModal(true);
  }, []);
  return (
    <Layout
      pageTitle="Order All"
      isLoaded
      breadcrumb={[
        {
          name: "Home",
          url: "/dashboard"
        },
        {
          name: "Order All"
        }
      ]}
    >
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <ReactTable
            data={order}
            defaultPageSize={5}
            filterable
            columns={[
              {
                Header: "Firstname",
                accessor: "user.firstname"
              },
              {
                Header: "Lastname",
                accessor: "user.lastname"
              },
              {
                Header: "From",
                accessor: "from"
              },
              {
                Header: "To",
                accessor: "to"
              },
              {
                Header: "Date",
                accessor: "date"
              },
              {
                Header: "Status",
                accessor: "status",
                Cell: props => {
                  let res = "";
                  if (props.value === ACTIVE) {
                    res = <Status theme="danger">Active</Status>;
                  } else if (props.value === DELIVERED) {
                    res = <Status theme="primary">DELIVERING</Status>;
                  } else if (props.value === COMPLETED) {
                    res = <Status theme="success">COMPLETED</Status>;
                  }
                  return res;
                }
              },
              {
                Header: "Notes",
                accessor: "notes"
              },
              {
                Header: "Created At",
                accessor: "createdAt",
                Cell: props =>
                  DateTime.fromISO(props.value).toLocaleString(
                    DateTime.DATE_MED
                  )
              },
              {
                Header: "Actions",
                accessor: "id",
                Cell: props => {
                  return (
                    <Row end="md">
                      <Col>
                        <Button
                          theme="success"
                          size="sm"
                          onClick={() =>
                            handleChange({
                              from: props.original.from,
                              to: props.original.to,
                              car: props.original.car
                            })
                          }
                        >
                          View Map
                        </Button>
                      </Col>
                    </Row>
                  );
                }
              }
            ]}
          />
        </>
      )}
    </Layout>
  );
}

export default OrderAll;
