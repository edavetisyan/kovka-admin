/* eslint-disable no-undef */
/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React, { useState, useCallback, useEffect } from "react";
import ReactTable from "react-table";
import Layout from "../../components/Layout";
import Button from "../../components/Custom/Button";
import { Col, Row } from "react-flexbox-grid";
import Modal from "../../components/Modal";
import fetchData from "../../utils/fetchData";
import Loading from "../../components/Loading";

function Product() {
  const [isLoading, setLoading] = useState(false);
  const [products, setProducts] = useState([]);
  const [modal, setModal] = useState(false);
  const [item, setItem] = useState("");
  useEffect(() => {
    setLoading(true);
    fetchData(`${process.env.REACT_APP_API_URL}products`, { method: "GET" })
      .then(res => setProducts(res.data))
      .catch(err => {
        console.log("err", err);
      })
      .finally(setLoading(false));
  }, []);
  const handleOpenModal = useCallback(e => {
    setItem(e);
    setModal(true);
  }, []);
  const handleDelete = useCallback(() => {
    fetchData(`${process.env.REACT_APP_API_URL}products/remove/${item}`, {
      method: "delete"
    })
      .then(() => {
        setModal(false);
      })
      .catch(e => {
        console.log(e);
      });
  }, [item]);
  return (
    <Layout
      pageTitle="Продукт"
      isLoaded
      breadcrumb={[
        {
          name: "Home",
          url: "/dashboard"
        },
        {
          name: "Продукт"
        }
      ]}
    >
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <ReactTable
            data={products}
            defaultPageSize={5}
            filterable
            columns={[
              {
                Header: "Image",
                accessor: "prImages",
                Cell: ({value}) => {
                  return(
                  <img
                    src={`${process.env.REACT_APP_API_URL}upload/${ value[0] ? value[0].filename : ''}`}
                    width="100"
                    height="100"
                    alt="products"
                    style={{ "object-fit": "contain" }}
                  />
                )}
              },
              {
                Header: "Category",
                accessor: "category.name"
              },
              {
                Header: "subCategory",
                accessor: "name"
              },
              {
                Header: "Имя",
                accessor: "name"
              },
              {
                Header: "Slug",
                accessor: "slug"
              },
              {
                Header: "Описание ",
                accessor: "description"
              },
              {
                Header: "Actions",
                accessor: "id",
                Cell: ({value}) => {
                  return (
                    <Row end="md">
                      <Col>
                        <Button
                          theme="success"
                          size="sm"
                          link={{
                            href: `/product/edit/${value}`
                          }}
                        >
                          Edit
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          theme="danger"
                          size="sm"
                          onClick={() => handleOpenModal(value)}
                        >
                          Delete
                        </Button>
                      </Col>
                    </Row>
                  );
                }
              }
            ]}
          />
          <Row end="md">
            <Button
              link={{
                href: "product/create"
              }}
            >
              Add
            </Button>
          </Row>
        </>
      )}
      {modal && (
        <Modal
          title="Delete"
          submitText="Delete"
          text="Are you sure?"
          onSubmit={() => handleDelete()}
          onClose={() => setModal(false)}
        />
      )}
    </Layout>
  );
}

export default Product;
