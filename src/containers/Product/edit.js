/* eslint-disable no-undef */
import React, { useMemo, useCallback, useState } from "react";
import PropTypes from "prop-types";
import Form from "../../components/Form";
import Layout from "../../components/Layout";
import ImageUpload from "../../components/Form/ImageUpload";
import Container from "../../components/Custom/Container";
import fetchData from "../../utils/fetchData";
import { useFetch } from "../../hooks/useFetch";

function EditProduct({ match, history }) {
  const cat = useFetch("categories");
  const data = useFetch(`products/${match.params.id}`);
  const [catId, setCatId] = useState(data.productDetail.category.id);
  const [image, setImage] = useState(data.prImages);
  const [isLoading, setLoading] = useState(false);
  const category = useMemo(() => {
    const res = [];
    cat.forEach(value => {
      res.push({
        label: value.name,
        value: value.id
      });
    });
    return res;
  }, [cat]);
  const handleImage = useCallback(e => {
    setImage(e);
  }, []);
  const subCategory = useMemo(() => {
    const data = cat.find(x => x.id === catId);
    const res = [];
    if(!data){
      return [];
    }
    data.subCategories.forEach(value => {
      res.push({
        label: value.name,
        value: value.id
      });
    });
    return res;
  }, [catId, cat]);
  const handleChange = useCallback(e => {
    setCatId(e.value);
  }, []);

  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      data.id = match.params.id;
      data.images = image;
      fetchData(`${process.env.REACT_APP_API_URL}products/update`, {
        body: data,
        method: "PUT"
      })
        .then(() => {
          history.push("/product");
        })
        .catch(e => {
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    },
    [history, match.params.id, image]
  );
  return (
    <Layout
      pageTitle="products"
      isLoaded
      breadcrumb={[
        {
          name: "products",
          url: "/products"
        },
        {
          name: "Products"
        }
      ]}
    >
      <Container>
      <ImageUpload 
        handleChagne={handleImage} 
        initialValue={ data.prImages[0] ?  data.prImages[0].filename : ''}
      />
        <Form
          content={[
            {
              componentType: "select",
              name: "catId",
              label: "Cateogries *",
              size: 6,
              handleChange,
              initialValue: data.productDetail.category.id,
              options: [...category],
              isRequired: true
            },
            {
              componentType: "select",
              name: "subCatId",
              label: "Sub Cateogries *",
              size: 6,
              initialValue: data.productDetail.subCategory.id,
              options: [...subCategory],
              isRequired: true
            },
            {
              componentType: "input",
              name: "name",
              label: "Имя *",
              initialValue: data.name,
              size: 6,
              isRequired: true
            },
            {
              componentType: "input",
              name: "slug",
              label: "Slug *",
              initialValue: data.slug,
              size: 6,
              isRequired: true
            },
            {
              componentType: "textarea",
              name: "description",
              label: "Описание *",
              initialValue: data.description,
              size: 12,
              isRequired: true
            },
            {
              componentType: "check",
              name: "last",
              id: "check1",
              initialValue: data.productDetail.new,
              label: "ПОСЛЕДНИЕ РАБОТЫ *",
              size: 12,
            }
          ]}
          button={{
            type: "submit",
            title: "submit"
          }}
          onSubmit={handleSubmit}
          isLoading={isLoading}
        />
      </Container>
    </Layout>
  );
}
EditProduct.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  history: PropTypes.shape({
    push: PropTypes.func
  })
};

export default EditProduct;
