/* eslint-disable no-undef */
import React, { useMemo, useCallback, useState } from "react";
import Form from "../../components/Form";
import PropTypes from "prop-types";
import Layout from "../../components/Layout";
import Container from "../../components/Custom/Container";
import fetchData from "../../utils/fetchData";
import ImageUpload from "../../components/Form/ImageUpload";
import { useFetch } from "../../hooks/useFetch";

function CreateProduct({ history }) {
  const [image, setImage] = useState([]);
  const [catId, setCatId] = useState('');

  const cat = useFetch("categories");

  const [isLoading, setLoading] = useState(false);
  const category = useMemo(() => {
    const res = [];
    cat.forEach(value => {
      res.push({
        label: value.name,
        value: value.id
      });
    });
    return res;
  }, [cat]);

  const subCategory = useMemo(() => {
    const data = cat.find(x => x.id === catId);
    const res = [];
    if(!data){
      return [];
    }
    data.subCategories.forEach(value => {
      res.push({
        label: value.name,
        value: value.id
      });
    });
    return res;
  }, [catId, cat]);

  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      data.images = image;
      fetchData(`${process.env.REACT_APP_API_URL}products/create`, {
        body: data
      })
        .then(() => {
          history.push("/product");
        })
        .catch(e => {
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    },
    [history, image]
  );
  const handleImage = useCallback(e => {
    setImage(e);
  }, []);
  const handleChange = useCallback(e => {
    setCatId(e.value);
  }, []);
  return (
    <Layout
      pageTitle="Продукт"
      isLoaded
      breadcrumb={[
        {
          name: "Продукт",
          url: "/product"
        },
        {
          name: "Создать"
        }
      ]}
    >
      <Container>
        <ImageUpload handleChagne={handleImage} />
        <Form
          content={[
            {
              componentType: "select",
              name: "catId",
              label: "Cateogries *",
              size: 6,
              options: [...category],
              handleChange,
              isRequired: true
            },
            {
              componentType: "select",
              name: "subCatId",
              label: "Sub Cateogries *",
              size: 6,
              options: [...subCategory],
              isRequired: true
            },
            {
              componentType: "input",
              name: "name",
              label: "Имя *",
              size: 6,
              isRequired: true
            },
            {
              componentType: "input",
              name: "slug",
              label: "Slug *",
              size: 6,
              isRequired: true
            },
            {
              componentType: "textarea",
              name: "description",
              label: "Описание *",
              size: 12,
              isRequired: true
            },
            {
              componentType: "check",
              name: "last",
              id: "check1",
              label: "ПОСЛЕДНИЕ РАБОТЫ *",
              size: 12
            }
          ]}
          button={{
            type: "submit",
            title: "submit"
          }}
          onSubmit={handleSubmit}
          isLoading={isLoading}
        />
      </Container>
    </Layout>
  );
}

CreateProduct.propTypes = {
  history: PropTypes.shape({ push: PropTypes.func }).isRequired
};
export default CreateProduct;
