/* eslint-disable no-undef */
import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";
import Form from "../../components/Form";
import Layout from "../../components/Layout";
import ImageUpload from "../../components/Form/ImageUpload";
import Container from "../../components/Custom/Container";
import fetchData from "../../utils/fetchData";
import { useFetch } from "../../hooks/useFetch";

function EditReviews({ match, history }) {
  const data = useFetch(`reviews/${match.params.id}`);
  const [isLoading, setLoading] = useState(false);
  const [image, setImage] = useState([{ filename: data.avatar }]);

  const handleImage = useCallback(e => {
    setImage(e);
  }, []);

  const handleSubmit = useCallback(
    data => {
      setLoading(true);
      data.id = match.params.id;
      data.avatar = image;
      fetchData(`${process.env.REACT_APP_API_URL}contact/update`, {
        body: data,
        method: "PUT"
      })
        .then(() => {
          history.push("/contact");
        })
        .catch(e => {
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    },
    [history, match.params.id, image]
  );
  return (
    <Layout
      pageTitle="reviews"
      isLoaded
      breadcrumb={[
        {
          name: "contact",
          url: "/contact"
        },
        {
          name: "контакт"
        }
      ]}
    >
      <Container>
        <ImageUpload
          handleChagne={handleImage}
          initialValue={data.avatar}
        />
        <Form
          content={[
            {
              componentType: "input",
              name: "name",
              label: "Имя *",
              initialValue: data.name,
              size: 6,
              isRequired: true
            },
            {
              componentType: "textarea",
              name: "phone",
              label: "Телефон *",
              initialValue: data.phone,
              size: 12,
              isRequired: true
            },
            {
              componentType: "textarea",
              name: "description",
              label: "Описание *",
              initialValue: data.description,
              size: 12,
              isRequired: true
            },
          ]}
          button={{
            type: "submit",
            title: "submit"
          }}
          onSubmit={handleSubmit}
          isLoading={isLoading}
        />
      </Container>
    </Layout>
  );
}
EditReviews.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  history: PropTypes.shape({
    push: PropTypes.func
  })
};

export default EditReviews;
