/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import Container, { Wrap, Box, Title, Action } from "../../components/Custom";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Form from "../../components/Form";

const menu = [
  {
    href: "/sign-in",
    name: "Sign In"
  }
];

function Forgot() {
  const [isLoading, setLoading] = useState(false);
  return (
    <div className="wrap">
      <Header />
      <div className="content">
        <Container>
          <Wrap>
            <Box>
              <Title
                title="Forgot Password"
              />
              <Form
                content={[
                  {
                    componentType: "input",
                    name: "email",
                    label: "Email",
                    isRequired: true,
                    size: 12
                  }
                ]}
                button={{
                  type: "submit",
                  title: "Reset password"
                }}
                // onSubmit={handleSubmit}
                isLoading={isLoading}
              />
              <Action text="Sign in for an account" menu={menu} />

            </Box>
          </Wrap>
        </Container>
      </div>
      <Footer />

    </div>
  );
}

export default Forgot;
