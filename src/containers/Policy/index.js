import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Container, {
  Wrap,
  Box,
  Title,
  Paragraph
} from "../../components/Custom";

function Policy() {
  return (
    <div className="wrap">
      <Header />
      <div className="content">
        <Title title="Policy" />
        <Container>
          <Wrap>
            <Box>
              <Paragraph center>
                It is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
                The point of using Lorem Ipsum is that it has a more-or-less
                normal distribution of letters, as opposed to using Content
                here, content here
              </Paragraph>
              <Paragraph center>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
              </Paragraph>
              <Paragraph center>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
              </Paragraph>
              <Paragraph center>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
              </Paragraph>
            </Box>
          </Wrap>
        </Container>
      </div>
      <Footer />
    </div>
  );
}
export default Policy;
