const changePlace = value => {
  let componentForm = {
    street_number: { type: "short_name", value: "" },
    route: { type: "short_name", value: "" },
    locality: { type: "long_name", value: "" },
    administrative_area_level_1: { type: "short_name", value: "" },
    country: { type: "short_name", value: "" },
    postal_code: { type: "short_name", value: "" }
  };
  for (var i = 0; i < value.address_components.length; i++) {
    let componentType = value.address_components[i].types[0];
    // eslint-disable-next-line no-prototype-builtins
    if (componentForm.hasOwnProperty(componentType)) {
      componentForm[componentType].value =
        value.address_components[i][componentForm[componentType].type];
    }
  }
  return componentForm;
};
export default changePlace;
