export class Errors extends Error {
  constructor(
    errorsMessage = null,
    errors = null,
    message = "NOT_FOUND",
    status = 404
  ) {
    super();
    this.message = message;
    this.errorsMessage = errorsMessage;
    this.errors = errors;
    this.status = status;
  }
}
