import storage from "./storage";
// import { Errors } from './error';

export default async (url, data = {}) => {
  const token = storage.get("token");
  const {
    method = "POST",
    body = {},
    headers = {
      "Content-Type": "application/json"
    }
  } = data;

  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }
  if (method !== "GET") {
    data.body = JSON.stringify(body);
  }
  return new Promise((resolve, reject) => {
    fetch(url, {
      ...data,
      method,
      headers
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        }
        reject(new Error(res.statusText));
      })
      .then(res => resolve(res))
      .catch(reject);
  });
};
