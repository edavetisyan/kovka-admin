import deliveryIcon1 from "../assets/img/deliver_icon1.png";
import deliveryIcon2 from "../assets/img/deliver_icon2.png";
import deliveryIcon3 from "../assets/img/deliver_icon3.png";
import deliveryIcon4 from "../assets/img/deliver_icon4.png";

import advantagesIcon1 from "../assets/img/privileges_icon1.png";
import advantagesIcon2 from "../assets/img/privileges_icon2.png";
import advantagesIcon3 from "../assets/img/privileges_icon3.png";
import advantagesIcon4 from "../assets/img/privileges_icon4.png";
import input_types from "./form";

const words = {
  hy: {
    header: {
      menu: ["ՄԵՐ ՄԱՍԻՆ", "ԻՆՉՊԵՍ ԱՌԱՔԵԼ", "ԱՌԱՎԵԼՈՒԹՅՈՒՆՆԵՐԸ", "ՀԵՏԱԴԱՐՁ ԿԱՊ"],
      btn: "ՄՈՒՏՔ"
    },
    about: {
      title: "ՄԵՐ ՄԱՍԻՆ",
      text:
        "UCAR-ը ՀՀ-ում առաջին առցանց հարթակն է, որը հնարավորություն է տալիս ուղարկել ծանրոցներ և բեռներ Հայաստանի և Արցախի տարբեր քաղաքներ: UCAR համակարգում գրանցվելուց հետո տեղադրեք Ձեր պատվերը և UCAR խմբի վարորդը կապ կհաստատի Ձեզ հետ։ UCAR-ն ազատում է բեռներ կամ իրեր ուղարկելու անհարմարություններից և ժամանակի վատնումից, վարորդների համար ստեղծում է հավելյալ եկամուտ վաստակելու հնարավարություն՝ համատեղելով իրենց ուղևորության հետ:"
    },
    delivery: {
      title: "ԻՆՉՊԵՍ ԱՌԱՔԵԼ",
      item: [
        {
          id: 1,
          img: deliveryIcon1,
          text: "Գրանցվե՛ք Ucar համակարգում"
        },
        {
          id: 2,
          img: deliveryIcon2,
          text: "Ընտրե՛ք ապրանքի տեղափոխման քաղաքը/վայրը"
        },
        {
          id: 3,
          img: deliveryIcon3,
          text: "Ընտրե՛ք առաքման ժամն ու ամսաթիվը"
        },
        {
          id: 4,
          img: deliveryIcon4,
          text: "Ընտրե՛ք տրանսպորտային միջոցը, հաստատե՛ք"
        }
      ]
    },
    advantages: {
      title: "ԱՌԱՎԵԼՈՒԹՅՈՒՆՆԵՐԸ",
      item: [
        {
          id: 1,
          img: advantagesIcon1,
          text: "Մատչելիություն"
        },
        {
          id: 2,
          img: advantagesIcon2,
          text: "Ապահովություն - Ընտրված վարորդներ և երաշխիքային մեխանիզմներ"
        },
        {
          id: 3,
          img: advantagesIcon3,
          text: "Լայն ծածկույթ - Հասանելիություն ՀՀ և Արցախի բոլոր քաղաքներում"
        },
        {
          id: 4,
          img: advantagesIcon4,
          text: "Հարմարավետություն - Առաքե՛ք և ստացե՛ք առանց տանից դուրս գալու"
        }
      ]
    },
    contactUs: {
      title: "ՀԵՏԱԴԱՐՁ ԿԱՊ",

      inputs: [
        {
          componentType: "input",
          name: "name",
          isRequired: true,
          size: 4,
          placeholder: "Անուն*"
        },
        {
          componentType: "input",
          placeholder: "Էլ․ փոստ*",
          name: "email",
          isRequired: true,
          size: 4,
          ...input_types.email
        },
        {
          componentType: "input",
          placeholder: "Թեմա*",
          name: "subject",
          isRequired: true,
          size: 4
        },
        {
          componentType: "textarea",
          placeholder: "Հաղորդագրություն*",
          name: "message",
          isRequired: true,
          size: 12
        }
      ],
      btn: "ՈւՂԱՐԿԵԼ"
    }
  },
  us: {
    header: {
      menu: ["ABOUT US", "HOW TO DELIVER", "ADVANTAGES", "CONTACT US"],
      btn: "SIGNIN"
    },
    about: {
      title: "ABOUT US",
      text:
        "UCAR is the first online platform in Armenia to deliver parcels and cargoes to different cities of Armenia and Artsakh. Once you have registered with UCAR, place your order and the UCAR team driver will contact you. UCAR removes the inconvenience and waste of time to ship cargo or items, creating the opportunity for drivers to earn extra money by combining their travel."
    },
    delivery: {
      title: "HOW TO DELIVER",
      item: [
        {
          id: 1,
          img: deliveryIcon1,
          text: "Get started in the Ucar system"
        },
        {
          id: 2,
          img: deliveryIcon2,
          text: "Select the city / location of shipping"
        },
        {
          id: 3,
          img: deliveryIcon3,
          text: "Select delivery time and date"
        },
        {
          id: 4,
          img: deliveryIcon4,
          text: "Choose the vehicle, confirm it"
        }
      ]
    },
    advantages: {
      title: "ADVANTAGES",
      item: [
        {
          id: 1,
          img: advantagesIcon1,
          text: "Availability"
        },
        {
          id: 2,
          img: advantagesIcon2,
          text: "Safety - Selected drivers and warranty mechanisms"
        },
        {
          id: 3,
          img: advantagesIcon3,
          text: "Broadband - Available in all cities of Armenia and Artsakh"
        },
        {
          id: 4,
          img: advantagesIcon4,
          text: "Convenience - Shipping and receiving without leaving home"
        }
      ]
    }
  },
  contactUs: {
    title: "CONTACT US",
    inputs: [
      {
        componentType: "input",
        name: "name",
        isRequired: true,
        size: 4,
        placeholder: "Name*"
      },
      {
        componentType: "input",
        placeholder: "Email*",
        name: "email",
        isRequired: true,
        size: 4,
        ...input_types.email
      },
      {
        componentType: "input",
        placeholder: "Subject*",
        name: "subject",
        isRequired: true,
        size: 4
      },
      {
        componentType: "textarea",
        placeholder: "Message*",
        name: "message",
        isRequired: true,
        size: 4
      }
    ],
    btn: "Send"
  }
};

export default words;
