export default {
  text: {
    validation: /.{2,}/
  },
  email: {
    type: "email",
    validation: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  },
  emailUsername: {
    type: "text",
    validation: /.{3,}/
  },
  username: {
    type: "text",
    validation: /.{6,}/
  },
  password: {
    validation: /.{8,}/,
    type: "password",
  },
  search: {
    validation: /.{2,}/,
  },

  textarea: {
    multiline: true
  },
  address: {
    validation: /.{2,}/,
    type: "text"
  }
};
