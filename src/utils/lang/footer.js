const footer = {
  hy: {
    faq: "ՀՏՀ",
    terms: "Օգտագործման կանոններ",
    privacy: "Գաղտնիություն",
    btn: "ԴԱՌՆԱԼ ԳՈՐԾԸՆԿԵՐ"
  },
  us: {
    faq: "Faq",
    terms: "Terms of",
    privacy: "Privacy",
    btn: "BECOME A PARTNER"
  }
};

export default footer;
