import input_types from "../form";
const signUp = {
  hy: {
    client: {
      inputs: [
        {
          componentType: "input",
          name: "firstname",
          label: "Անուն *",
          size: 6,
          isRequired: true
        },
        {
          componentType: "input",
          name: "lastname",
          label: "Ազգանուն *",
          size: 6,
          isRequired: true
        },
        {
          componentType: "phone",
          name: "phone",
          label: "Հեռախոսահամար *",
          size: 6,
          isRequired: true
        },
        {
          componentType: "input",
          name: "email",
          label: "Էլ․ փոստ*",
          size: 6,
          isRequired: true,
          ...input_types.email
        },
        {
          componentType: "input",
          name: "password",
          label: "Ծածկագիր",
          isRequired: true,
          size: 6,
          ...input_types.password
        },
        {
          componentType: "input",
          name: "confirmPassword",
          label: "Կրկնել ծածկագիրը",
          isRequired: true,
          size: 6,
          ...input_types.password
        }
      ]
    },
    driver: {},

    btn: "Հաստատել"
  },
  us: {
    client: {
      inputs: [
        {
          componentType: "phone",
          name: "phone",
          label: "Phone *",
          size: 12,
          isRequired: true
        },
        {
          componentType: "input",
          name: "email",
          label: "Email *",
          size: 6,
          isRequired: true,
          ...input_types.email
        },
        {
          componentType: "input",
          name: "firstname",
          label: "First Name *",
          size: 12,
          isRequired: true
        },
        {
          componentType: "input",
          name: "lastname",
          label: "Last Name*",
          size: 12,
          isRequired: true
        },
        {
          componentType: "input",
          name: "password",
          label: "Password *",
          isRequired: true,
          size: 12,
          ...input_types.password
        },
        {
          componentType: "input",
          name: "confirmPassword",
          label: "Confirm Password",
          isRequired: true,
          size: 12,
          ...input_types.password
        }
      ]
    },
    driver: {},

    btn: "Submit"
  }
};

export default signUp;
