import input_types from "../form";

const signIn = {
  hy: {
    inputs: [
      {
        componentType: "input",
        name: "email",
        label: "Почта *",
        size: 12,
        isRequired: true
      },
      {
        componentType: "input",
        name: "password",
        label: "пароль",
        isRequired: true,
        size: 12,
        ...input_types.password
      }
    ],
    btn: "Հաստատել"
  },
  us: {
    inputs: [
      {
        componentType: "phone",
        name: "phone",
        label: "Phone *",
        size: 12,
        isRequired: true
      },
      {
        componentType: "input",
        name: "password",
        label: "Password",
        isRequired: true,
        size: 12,
        ...input_types.password
      }
    ],
    btn: "Submit"
  }
};

export default signIn;
