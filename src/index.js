import React from "react";
import ReactDOM from "react-dom";
import "./assets/css/index.scss";
import Root from "./routes";
import { AuthContextProvider } from "./stores/auth";
import { SettingsContextProvider } from "./stores/settings";
import history from "./utils/history";
import * as serviceWorker from "./serviceWorker";
ReactDOM.render(
  <SettingsContextProvider>
    <AuthContextProvider>
      <Root history={history} />
    </AuthContextProvider>
  </SettingsContextProvider>,
  // eslint-disable-next-line no-undef
  document.getElementById("root")
);

serviceWorker.unregister();
