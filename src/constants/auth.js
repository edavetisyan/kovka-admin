export const SET_USER = "SET_USER";
export const SIGN_IN_ANONYMOUSLY = "SIGN_IN_ANONYMOUSLY";
export const LOG_OUT = "LOG_OUT";
