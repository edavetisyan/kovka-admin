export const ADMIN = "1";
export const DRIVER = "2";
export const CLIENT = "3";

export const UNCONFIRMED = "1";
export const UNCOMPLETED = "2";
export const WAITING = "3";
export const ACTIVE = "4";
