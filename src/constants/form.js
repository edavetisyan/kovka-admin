export const SET_VALUE = "SET_VALUE";
export const CHANGE_FOCUS = "CHANGE_FOCUS";
export const SET_FOCUS = "SET_FOCUS";
export const SHOW_ERRORS = "SHOW_ERRORS";
