import storage from "../utils/storage";

const NOT_EXIST = 0;
const PENDING = 1;
const REJECT = 2;
const RESOLVE = 3;

function objectToUrlParams(object = {}) {
  return (
    "?" +
    Object.keys(object)
      .filter(item => object.hasOwnProperty(item) && object[item] !== undefined)
      .reduce(
        (accumulator, currentKey) =>
          accumulator.concat([
            `${currentKey}=${encodeURIComponent(object[currentKey])}`
          ]),
        []
      )
      .join("&")
  );
}

const cache = {};

export function useFetch(url, options = {}) {
  const token = storage.get("token");
  const {
    method = "GET",
    headers = {
      "Content-Type": "application/json"
    },
    query = {}
  } = options;
  const queryStr = objectToUrlParams(query);
  const {
    [url + method + queryStr]: {
      status = NOT_EXIST,
      data = {},
      promise: promiseOld = null,
      // error = null,
      time
    } = {}
  } = cache;
  if (status === PENDING) {
    throw promiseOld;
  }
  if (status === REJECT) {
    delete cache[url + method + queryStr];
    return {};
  }
  if (status === RESOLVE && Date.now() < time) {
    return data;
  }
  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }
  const promise = new Promise((resolve, reject) => {
    const timerId = setTimeout(function() {
      reject(new Error("timeout"));
    }, 10000);
    fetch(`${process.env.REACT_APP_API_URL}${url}${queryStr}`, {
      headers,
      ...options
    })
      .then(response => {
        clearTimeout(timerId);
        if (response.ok) {
          return response.json();
        }
        reject(new Error(response.statusText));
      })
      .then(data => resolve(data.data))
      .catch(error => reject(error));
  });
  promise
    .then(data => {
      cache[url + method + queryStr] = {
        data,
        status: RESOLVE,
        error: null,
        promise: null,
        time: Date.now() + 300000
      };
    })
    .catch(error => {
      cache[url + method + queryStr] = {
        data: null,
        status: REJECT,
        error,
        promise: null,
        time: 0
      };
    });
  cache[url + method + queryStr] = {
    data: null,
    status: PENDING,
    error: null,
    promise,
    time: 0
  };
  throw promise;
}

export function clearCache(url, method = "GET", queryStr) {
  delete cache[url + method + queryStr];
}
