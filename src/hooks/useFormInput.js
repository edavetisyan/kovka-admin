import { useState } from "react";
export function useFormInput(initValue) {
  const [value, setValue] = useState(initValue);
  return {
    value,
    onChange: text => setValue(text)
  };
}
