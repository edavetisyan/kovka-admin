import api from "../../api";
import storage from "../utils/storage";

const PENDING = 1;
const REJECT = 2;
const RESOLVE = 3;

let caches = {};

export function useAsyncStorage(key = "token", checkUserData = false) {
  const {
    promise: promiseOld = null,
    status = 0,
    data = {},
    error = null
  } = caches;

  if (status === RESOLVE) {
    // caches = {};
    return data;
  }
  if (status === REJECT) {
    // caches = {};
    throw error;
  }
  if (status === PENDING) {
    // pending
    throw promiseOld;
  }
  const promise = new Promise((resolve, reject) => {
    const token = storage.get(key);
    if (key === "token" && checkUserData && token !== null) {
      const timerId = setTimeout(function() {
        reject(new Error("timeout"));
      }, 10000);
      fetch(`${api.url}user/me`, {
        headers: {
          Authorization: token
        }
      })
        .then(response => {
          clearTimeout(timerId);
          if (response.ok) {
            return response.json();
          }
          reject(new Error(response.statusText));
        })
        .then(data => {
          resolve({ user: data.data, token });
        })
        .catch(error => {
          reject(error);
        });
    } else {
      resolve(token);
    }
  });
  promise
    .then(data => {
      caches = {
        data,
        status: RESOLVE,
        error: null,
        promise: null
      };
    })
    .catch(error => {
      caches = {
        data: null,
        status: REJECT,
        error,
        promise: null
      };
    });
  caches = {
    data: null,
    status: PENDING,
    error: null,
    promise
  };
  throw promise;
}
