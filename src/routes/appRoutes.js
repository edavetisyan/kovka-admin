/* eslint-disable no-unused-vars */
import React from "react";
import PropTypes from "prop-types";
import { Router, Switch } from "react-router-dom";
import { UnAuthedRoute, AuthedRoute, AppRoute } from "../components/Routing";

import SignIn from "../containers/SignIn";
import Forgot from "../containers/Forgot";
import Dashboard from "../containers/Dashboard";
import Logout from "../components/Logout";

import Product from "../containers/Product";
import EditProduct from "../containers/Product/edit";
import CreateCare from "../containers/Product/create";

import Category from "../containers/Category";
import CategoryCreate from "../containers/Category/create";
import EditCategory from "../containers/Category/edit";

import Reviews from "../containers/Reviews";
import CreateReviews from "../containers/Reviews/create";
import EditReviews from "../containers/Reviews/edit";

import SubCategory from "../containers/SubCategory";
import SubCategoryCreate from "../containers/SubCategory/create";
import SubCategoryEdit from "../containers/SubCategory/edit";
//Users

import Contact from "../containers/Contact";
import EditContact from "../containers/Contact/edit";

function AppRoutes({ history, isLoading }) {
  return (
    <Router history={history}>
      <Switch>
        <AppRoute path="/" component={SignIn} exact />
        <AuthedRoute
          path="/dashboard"
          isUserLoaded={isLoading}
          component={Dashboard}
          exact
        />
        <UnAuthedRoute path="/forgot" component={Forgot} exact />
        <AuthedRoute
          path="/product"
          component={Product}
          exact
        />
        <AuthedRoute
          path="/product/create"
          component={CreateCare}
          exact
        />
        <AuthedRoute
          path="/product/edit/:id"
          isUserLoaded={isLoading}
          component={EditProduct}
          exact
        />
        <AuthedRoute
          path="/category"
          component={Category}
          exact
        />
        <AuthedRoute
          path="/category/create"
          component={CategoryCreate}
          exact
        />
        <AuthedRoute
          path="/category/edit/:id"
          component={EditCategory}
          exact
        />
        <AuthedRoute
          path="/sub-category"
          component={SubCategory}
          exact
        />
        <AuthedRoute
          path="/sub-category/create"
          component={SubCategoryCreate}
          exact
        />
        <AuthedRoute
          path="/sub-category/edit/:id"
          component={SubCategoryEdit}
          exact
        />
        <AuthedRoute
          path="/reviews"
          component={Reviews}
          exact
        />
        <AuthedRoute
          path="/reviews/create"
          component={CreateReviews}
          exact
        />
        <AuthedRoute
          path="/reviews/edit/:id"
          component={EditReviews}
          exact
        />
        <AuthedRoute
          path="/conatct"
          component={Contact}
          exact
        />
        <AuthedRoute
          path="/conatct/edit/:id"
          component={EditContact}
          exact
        />
        <AuthedRoute
          path="/logout"
          component={Logout}
          exact
        />
      </Switch>
    </Router>
  );
}

AppRoutes.propTypes = {
  history: PropTypes.shape({}).isRequired,
  user: PropTypes.object.isRequired,
  isLoading: PropTypes.bool
};

export default AppRoutes;
