import React, { Suspense, useContext, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Router } from "react-router-dom";
import AppRoutes from "./appRoutes";
import ErrorBoundary from "../components/ErrorBoundary";
import "react-table/react-table.css";
import Loading from "../components/Loading";
import { AuthContext } from "../stores/auth";
import { SET_USER } from "../constants/auth";
import storage from "../utils/storage";
import fetchData from "../utils/fetchData";

const Root = ({ history }) => {
  const { state, dispatch } = useContext(AuthContext);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (storage.get("token")) {
      // eslint-disable-next-line no-undef
      fetchData(`${process.env.REACT_APP_API_URL}auth/me`, { method: "GET" })
        .then(res => {
          dispatch({
            type: SET_USER,
            payload: res.data
          });
          // eslint-disable-next-line react/prop-types
          history.push("/dashboard");
          setLoading(true);
        })
        .catch(e => {
          // eslint-disable-next-line no-undef
          console.log("e", e);
        })
        .finally(() => setLoading(false));
    }
  }, [dispatch, history]);
  return (
    <Router history={history}>
      <ErrorBoundary>
        {isLoading ? (
          <Loading />
        ) : (
          <Suspense fallback={<Loading />}>
            <AppRoutes
              history={history}
              user={state.user}
              isLoading={isLoading}
            />
          </Suspense>
        )}
      </ErrorBoundary>
    </Router>
  );
};
Root.propTypes = {
  history: PropTypes.shape({}).isRequired
};

export default Root;
