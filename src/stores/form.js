import React, { createContext, useReducer } from "react";
import {
  SET_VALUE,
  CHANGE_FOCUS,
  SET_FOCUS,
  SHOW_ERRORS
} from "../constants/form";

const FormContext = createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case SET_VALUE:
      return {
        ...state,
        elements: {
          ...state.elements,
          [action.payload.name]: {
            value: action.payload.text,
            error: action.payload.error
          }
        }
      };
    case CHANGE_FOCUS: {
      const index = state.inputsArray.findIndex(
        item => item === action.payload.name
      );
      return { ...state, focused: state.inputsArray[index + 1] };
    }
    case SET_FOCUS: {
      return { ...state, focused: action.payload.name };
    }
    case SHOW_ERRORS: {
      return { ...state, showErrors: true };
    }
    default:
      return state;
  }
};

function FormContextProvider(props) {
  const { elements, inputsArray } = props;
  const [state, dispatch] = useReducer(reducer, {
    elements,
    inputsArray,
    focused: null,
    showErrors: false
  });
  const value = { state, dispatch };
  return (
    <FormContext.Provider value={value}>{props.children}</FormContext.Provider>
  );
}

const FormContextConsumer = FormContext.Consumer;
export { FormContext, FormContextProvider, FormContextConsumer };
