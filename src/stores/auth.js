import React, { createContext, useReducer } from "react";
import PropTypes from "prop-types";
import { SET_USER, SIGN_IN_ANONYMOUSLY, LOG_OUT } from "../constants/auth";
const AuthContext = createContext();
const initialState = {
  user: {},
  token: "",
  firebaseId: "",
  authenticated: false,
};

const reducer = (state, action) => {
  switch (action.type) {
    case LOG_OUT:
      return { ...initialState };
    case SET_USER:
      return {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
        authenticated: true,
      };
    case SIGN_IN_ANONYMOUSLY:
      return { ...state, firebaseId: action.payload };
    default:
      return state;
  }
};

function AuthContextProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value = { state, dispatch };
  return (
    <AuthContext.Provider value={value}>{props.children}</AuthContext.Provider>
  );
}

AuthContextProvider.propTypes = {
  children: PropTypes.node.isRequired
};

const AuthContextConsumer = AuthContext.Consumer;
export { AuthContext, AuthContextProvider, AuthContextConsumer };
