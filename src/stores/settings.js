import React, { createContext, useReducer } from "react";
import PropTypes from "prop-types";
import {
  CHANGE_LANGUAGE,
  CHANGE_SWITCHER,
  CHANGE_PERMISSION
} from "../constants/settings";

const SettingsContext = createContext();
const initialState = {
  language: "hy",
  newProductNotification: false,
  statusNotification: false
};

const reducer = (state, action) => {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      return { ...state, language: action.payload };
    case CHANGE_SWITCHER: {
      const { name } = action.payload;

      state[name] = !state[name];
      return { ...state };
    }
    case CHANGE_PERMISSION:
      return {
        ...state,
        newProductNotification: action.payload,
        statusNotification: action.payload
      };
    default:
      return state;
  }
};

function SettingsContextProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value = { state, dispatch };
  return (
    <SettingsContext.Provider value={value}>
      {props.children}
    </SettingsContext.Provider>
  );
}

SettingsContextProvider.propTypes = {
  children: PropTypes.node.isRequired
};

const SettingsContextConsumer = SettingsContext.Consumer;
export { SettingsContext, SettingsContextProvider, SettingsContextConsumer };
