import React, { createContext, useReducer } from "react";
import { CHANGE_ADDRESS, CHANGE_COORDS } from "../constants/map";
const MapContext = createContext();
const initialState = {
  address: "",
  city: "",
  coords: {
    latitude: 25.276987,
    longitude: 55.296249
  }
};

const reducer = (state, action) => {
  switch (action.type) {
    case CHANGE_ADDRESS:
      return {
        ...state,
        address: action.payload.line1,
        city: action.payload.city
      };
    case CHANGE_COORDS:
      return { ...state, coords: action.payload };
    default:
      return state;
  }
};

function MapContextProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value = { state, dispatch };
  return (
    <MapContext.Provider value={value}>{props.children}</MapContext.Provider>
  );
}

const MapContextConsumer = MapContext.Consumer;
export { MapContext, MapContextProvider, MapContextConsumer };
